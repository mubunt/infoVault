
# APPLICATION: *infoVault*

**infoVault** is the my way to take completely secure some information. All pieces of information are stored in a database. Each note is encrypted using a key you provide. Without this key, your information is completely safe and secure. Encryption and decryption are provided by the Cipher class, core of the Java Cryptographic Extension (JCE) framework. The requested transformation is described as **AES/ECB/PKCS5PADDING**:

- Cipher Algorithm Names: **AES** - Advanced Encryption Standard as specified by NIST in FIPS 197. Also known as the Rijndael algorithm by Joan Daemen and Vincent Rijmen, AES is a 128-bit block cipher supporting keys of 128, 192, and 256 bits (128 bits in our case).
- Cipher Algorithm Modes: **ECB** - Electronic Codebook Mode, as defined in FIPS PUB 81 .
- Cipher Algorithm Padding: **PKCS5PADDING** - The padding scheme described in RSA Laboratories, "PKCS #5: Password-Based Encryption Standard," version 1.5, November 1993.

Information managed by this application is only textual. However, images can be inserted. Information can be presented as free text or in grid form.

When the application is launched, the first tab summarizes, in thumbnail form,  the images inserted in the various information (refer to Example #5) . By right clicking on an image, it is possible to export the original image in PNG form.

***
![Attention](README_images/warning.png  "Attention") **What happens if I lose my encryption key?** There is a strong probability that you no longer access your information .... You have to do several tests, their number is not limited by the application. However, you can send me an email (mubunt@gmail.com), I will reply with a pointer on a software that will help you to retrieve this key.
***

**Example 1:** Startup Screen: Definition of the encryption key.
![infoVault](README_images/infoVault-01.png  "infoVault")

**Example 2:** Changing encryption key.
![infoVault](README_images/infoVault-03.png  "infoVault - Information selection")

**Example 3:** Version and license.
![infoVault](README_images/infoVault-04.png  "infoVault - Information selection")

**Example 4:** Selecting information to display / edit or define new information.
![infoVault](README_images/infoVault-05.png  "infoVault - Information selection")

**Example 5:** 
![infoVault](README_images/infoVault-06.png  "infoVault - Example")

**Example 6:** (not updated)
![infoVault](README_images/infoVault-09.png  "infoVault - Example")

Modified information is indicated by an "\*" in the name of the tab. The *Remove* button is used to delete the information from the database and the *Save* button to make the information permanent. The name of the information can be changed and must then be validated via the "OK".

## EDITION CAPABILITIES
![infoVault](README_images/infoVault-07.png  "infoVault - Edition capabilities")
For text, selected characters can be set to bold (button #2) or italic (button #2), underlined (button #3), striked out (button #4) and / or colored (button #5). The background of the page can be also colored (button #6). Character font can be changed (button #7) and this change is applied to all the text. Images can also be inserted (button #8); recognized  formats are PNG, GIF and JPEG. The button #9 allows to consider the selected string as an URL. After that, with CTRL +  mouse click on any character of this string, the URL is opened in our favourte browser.

For grid, bold, italic, background, foreground, fonts and URL are available.

## LICENSE

**infoVault** is covered by the GNU General Public License (GPL) version 3 and above.

The icons come from the Web (among others from Tango Icon Theme) and are released in the public domain.

The splashscreen image also comes from the Web. It is supposed to be in the "public domain". I do not intend to violate the rights of legitimate intellectual, artistic rights or copyright. If you are the rightful owner of this image, so please contact me and I will do whatever is necessary both for the image to be removed or give credit where it's due.

## USAGE infoVault

``` Bash
	$ infoVault --help
	infoVault - Copyright (c) 2016-2018, Michel RIZZO. All Rights Reserved.
	infoVault - Version 4.9.0
	
	Management of sensitive information at home.
	
	Usage: infoVault [OPTIONS]...
	
	  -h, --help     Print help and exit
	  -V, --version  Print version and exit
	$ 
```

## STRUCTURE OF THE APPLICATION
This section walks you through **infoVault**'s structure. Once you understand this structure, you will easily find your way around in **infoVault**'s code base.

``` Bash
$ yaTree
./                                                                # Application level
├── README_images/                                                # Images for documentation
│   ├── infoVault-01.png                                          # -- Screenshot infoVault
│   ├── infoVault-03.png                                          # -- Screenshot infoVault
│   ├── infoVault-04.png                                          # -- Screenshot infoVault
│   ├── infoVault-05.png                                          # -- Screenshot infoVault
│   ├── infoVault-06.png                                          # -- Screenshot infoVault
│   ├── infoVault-07.png                                          # -- Screenshot infoVault
│   ├── infoVault-09.png                                          # -- Screenshot infoVault
│   ├── vault.png                                                 # 
│   └── warning.png                                               # -- Warning icon for README.md file
├── infoVault_bin/                                                # Binary directory: jars (third-parties and local) and driver
│   ├── images/                                                   # -- Images (icons) directory
│   │   ├── .....                                                 #
│   │   └── infoVaultBack.jpeg                                    # 
│   ├── Makefile                                                  # -- Makefile
│   ├── commons-cli-1.4.jar                                       # -- Third-party jar file
│   ├── commons-lang3-3.9.jar                                     # -- Third-party jar file
│   ├── org.eclipse.jface_3.18.0.v20191122-2109.jar               # -- Third-party jar file
│   ├── org.eclipse.nebula.widgets.gallery_1.0.0.201912241810.jar # -- Third-party jar file
│   ├── org.eclipse.nebula.widgets.grid_1.1.0.201912241810.jar    # -- Third-party jar file
│   ├── org.eclipse.nebula.widgets.pshelf_1.1.0.201912241810.jar  # -- Third-party jar file
│   ├── sqlite-jdbc-3.27.2.1.jar                                  # -- Third-party jar file
│   └── swt-linux-415.jar                                         # -- Third-party jar file
├── infoVault_c/                                                  # C Source directory
│   ├── Makefile                                                  # -- Makefile
│   ├── infoVault.c                                               # -- C main source file
│   └── infoVault.ggo                                             # -- 'gengetopt' option definition
│                                                                 # -- Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
├── infoVault_java/                                               # JAVA Source directory
│   ├── infoVault/                                                # -- IntelliJ infoVault project structure
│   │   ├── src/                                                  # 
│   │   │   ├── META-INF/                                         # 
│   │   │   ├── infoVault.java                                    # 
│   │   │   ├── infoVaultAES.java                                 # 
│   │   │   ├── infoVaultAbout.java                               # 
│   │   │   ├── infoVaultBuildInfo.java                           # 
│   │   │   ├── infoVaultCacheColors.java                         # 
│   │   │   ├── infoVaultCacheFonts.java                          # 
│   │   │   ├── infoVaultCacheImages.java                         # 
│   │   │   ├── infoVaultDB.java                                  # 
│   │   │   ├── infoVaultDBimg.java                               # 
│   │   │   ├── infoVaultEditor.java                              # 
│   │   │   ├── infoVaultGallery.java                             # 
│   │   │   ├── infoVaultGrid.java                                # 
│   │   │   ├── infoVaultInformation.java                         # 
│   │   │   ├── infoVaultKeyChange.java                           # 
│   │   │   ├── infoVaultLogOn.java                               # 
│   │   │   └── infoVaultUI.java                                  # 
│   │   └── infoVault.iml                                         # 
│   └── Makefile                                                  # -- Makefile
├── COPYING.md                                                    # GNU General Public License markdown file
├── LICENSE.md                                                    # License markdown file
├── Makefile                                                      # Top-level makefile
├── README.md                                                     # ReadMe Mark-Down file
├── RELEASENOTES.md                                               # Release Notes markdown file
└── VERSION                                                       # Version identification text file

8 directories, 60 files
$
```

## HOW TO BUILD THIS APPLICATION

``` Bash
$ cd infoVault
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION

``` Bash
$ cd infoVault
$ make release
$ infoVault
	# Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    # We consider that $BIN_DIR is a part of the PATH.
```

## HOW TO PLAY WITH JAVA SOURCES
To play with, we recommend to use **IntelliJ IDEA**, on a Linux platform:

- Launch IntelliJ IDEA
- Click on **File -> Open...** and select *infoVault/infoVault_java/infoVault* project
- It's your turn...

## SOFTWARE REQUIREMENTS
- For usage:
  - JAVA 1.8.0 for usage and development
- For development:
  - IntelliJ IDEA 2019.2.4 (Community Edition) Build #IC-192.7142.36, built on October 29, 2019
  - Openjdk:
    - version "1.8.0_232-ea"
    - OpenJDK Runtime Environment (build 1.8.0_232-ea-8u232-b09-0ubuntu1-b09)
    - OpenJDK 64-Bit Server VM (build 25.232-b09, mixed mode)
  - GNU gengetopt 2.22.6 

Application developed and tested with XUBUNTU 18.04.

## RELEASE NOTES
Refer to file [RELEASENOTES](RELEASENOTES.md) .

***