#-------------------------------------------------------------------------------
# Copyright (c) 2016-2019, Michel RIZZO (the 'author' in the following)
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Project: infoVault
# Management of sensitive information at home
#-------------------------------------------------------------------------------
PROGRAM			= infoVault
BINJAR			= ../$(PROGRAM)_bin
IDEA_PRJ		= $(PROGRAM)
COMMONSCLI		= $(notdir $(shell ls $(BINJAR)/commons-cli-*))
COMMONSLANG		= $(notdir $(shell ls $(BINJAR)/commons-lang3-*))
JFACE 			= $(notdir $(shell ls $(BINJAR)/org.eclipse.jface_*))
GALLERY 		= $(notdir $(shell ls $(BINJAR)/org.eclipse.nebula.widgets.gallery_*))
GRID 			= $(notdir $(shell ls $(BINJAR)/org.eclipse.nebula.widgets.grid_*))
PSHELF 			= $(notdir $(shell ls $(BINJAR)/org.eclipse.nebula.widgets.pshelf_*))
SQLITE 			= $(notdir $(shell ls $(BINJAR)/sqlite-jdbc-*))
SWT				= $(notdir $(shell ls $(BINJAR)/swt-linux-*))
JARS			= $(COMMONSCLI) $(COMMONSLANG) $(JFACE) $(GALLERY) $(GRID) $(PSHELF) $(SQLITE) $(SWT)
VERSINFO		= $(IDEA_PRJ)/src/infoVaultBuildInfo.java
MOD_VERSINFO	= $(IDEA_PRJ)/src/modified_infoVaultBuildInfo.java
SOURCES			= $(IDEA_PRJ)/src/infoVault.java \
				  $(IDEA_PRJ)/src/infoVaultAbout.java \
				  $(IDEA_PRJ)/src/infoVaultAES.java \
				  $(IDEA_PRJ)/src/infoVaultCacheColors.java \
				  $(IDEA_PRJ)/src/infoVaultCacheFonts.java \
				  $(IDEA_PRJ)/src/infoVaultCacheImages.java \
				  $(IDEA_PRJ)/src/infoVaultDB.java \
				  $(IDEA_PRJ)/src/infoVaultDBimg.java \
				  $(IDEA_PRJ)/src/infoVaultEditor.java \
				  $(IDEA_PRJ)/src/infoVaultGallery.java \
				  $(IDEA_PRJ)/src/infoVaultGrid.java \
				  $(IDEA_PRJ)/src/infoVaultInformation.java \
				  $(IDEA_PRJ)/src/infoVaultKeyChange.java \
				  $(IDEA_PRJ)/src/infoVaultLogOn.java \
				  $(IDEA_PRJ)/src/infoVaultUI.java

include ../.make/java.mk
#-------------------------------------------------------------------------------
