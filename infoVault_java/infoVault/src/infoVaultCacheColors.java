//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;

import java.util.ArrayList;
//------------------------------------------------------------------------------
class infoVaultCacheColors {
	private static final ArrayList<Color> colors = new ArrayList<>();
	//--------------------------------------------------------------------------
	static Color get(int r, int g, int b) {
		Color $ = new Color(Display.getCurrent(), r, g, b);
		colors.add($);
		return $;
	}

	static Color get(RGB rgb) {
		Color $ = new Color(Display.getCurrent(), rgb);
		colors.add($);
		return $;
	}
	//--------------------------------------------------------------------------
	static void disposeAll() {
		for (int k = 0; k < colors.size(); ++k) {
			if (colors.get(k) == null) continue;
			colors.get(k).dispose();
			colors.set(k, null);
		}
	}
}
//==============================================================================
