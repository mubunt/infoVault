//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.apache.commons.cli.*;
//------------------------------------------------------------------------------
class infoVault {
	private static final String eol = System.getProperty("line.separator");
	private static final String slash = System.getProperty("file.separator");
	private static final String jarFile = "infoVault.jar";
	private static final String intelliJobjpath = "infoVault_java/infoVault/out/production/infoVault/";

	static final String sError = "!!! ERROR !!! ";
	static final String toolName = "infoVault";
	static final String sDescr= "Management of sensitive information @ home";
	static final String sTitle = toolName + ", " + sDescr;
	static final String sEncryptionKeyTitle = "Encryption Keys";
	static final String sAboutTitle = "About this tool";
	static final String sAboutTool = "Copyright (c) 2016-2018, 2020, Michel RIZZO" + eol + eol
			+ "This program is free software; you can redistribute it and/or modify it under the terms" + eol
			+ "of the GNU General Public License as published by the Free Software Foundation; either" + eol
			+ "version 2 of the License, or (at your option) any later version." + eol + eol
			+ "This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;" + eol
			+ "without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE." + eol
			+ "See the GNU General Public License for more details." + eol + eol
			+ "You should have received a copy of the GNU General Public License along with this program;" + eol
			+ "if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor," + eol
			+ "Boston, MA 02110-1301, USA.";
	static final boolean TOBEUNLOCKED = true;
	static int releaseMarker;
	//--------------------------------------------------------------------------
	public static void main(String[] args) {
		// ---- PATH OF THIS JAR
		releaseMarker = 0;
		String InstallRootDir = infoVault.class.getProtectionDomain().getCodeSource().getLocation().getPath().replace(jarFile, "");
		if (InstallRootDir.length() > intelliJobjpath.length()) {
			if (intelliJobjpath.equals(InstallRootDir.substring(InstallRootDir.length() - intelliJobjpath.length()))) {
				InstallRootDir = InstallRootDir.substring(0, InstallRootDir.length() - intelliJobjpath.length() - 1) + slash + "infoVault_bin";
				releaseMarker = 1;
			}
		}
		// ---- OPTIONS PROCESSING
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		options.addOption("h", "help", false, "print this message and exit");
		options.addOption("V", "version", false, "print the version information and exit");
		try {
			CommandLine line = parser.parse(options, args);
			// Option: Help
			if (line.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp(sTitle, options);
				Exit(false, 0);
			}
			// Option: Version
			if (line.hasOption("version")) {
				System.out.println(jarFile +
				 	"	Version: " + infoVaultBuildInfo.getVersion() +
					" - Build: " + infoVaultBuildInfo.getNumber() +
					" - Date: " + infoVaultBuildInfo.getDate());
				Exit(false, 0);
			}
		} catch (ParseException exp) {
			Error("Parsing failed - " + exp.getMessage());
		}
		// ---- LAUNCH USER INTERFACE
		infoVaultLogOn.LogOn(InstallRootDir);
		// ---- EXIT
		Exit(false, 0);
	}
	//--------------------------------------------------------------------------
	private static void Error(String s) {
		System.err.println(sError + s);
		Exit(false, -1);
	}
	//--------------------------------------------------------------------------
	static void Exit(boolean unlock, int value) {
		if (unlock)
			infoVaultDB.unlockDB();
		infoVaultLogOn.Exit();
		infoVaultUI.Exit();
		infoVaultDB.Exit();
		infoVaultDBimg.Exit();
		System.exit(value);
	}
}
//==============================================================================
