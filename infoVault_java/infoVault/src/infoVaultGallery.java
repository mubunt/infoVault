//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

class infoVaultGallery {
	/* **** REMOVE AUTOMATIC ADDING OF A GALLERY LISTING ALL IMAGES USED IN THE VAULT.
	//--------------------------------------------------------------------------
	static class anImage {
		GalleryItem imageItem;
		String imageName;
		String imageIndex;
	}
	private static ArrayList<anImage> Images;
	private static int CurrentGalleryItem;
	private static Gallery gallery;
	//--------------------------------------------------------------------------
	static void displayGallery(Composite parent, infoVaultUI.anInformationHeader[] listInformation) {
		Images = new ArrayList<>();
		gallery = new Gallery(parent, SWT.V_SCROLL | SWT.MULTI);

		DefaultGalleryGroupRenderer gr = new DefaultGalleryGroupRenderer();
		gr.setTitleBackgroundGradient(infoVaultLogOn.GALLERYITEMCOLOR_FROM, infoVaultLogOn.GALLERYITEMCOLOR_TO);
		gr.setTitleForeground(infoVaultLogOn.GALLERYITEMTITLECOLOR);
		gallery.setGroupRenderer(gr);

		DefaultGalleryItemRenderer ir = new DefaultGalleryItemRenderer();
		gallery.setItemRenderer(ir);

		for (infoVaultUI.anInformationHeader information : listInformation) {
			String[] listIndex = infoVaultDBimg.getIndex(information.getHeader());
			if (listIndex.length != 0) {
				GalleryItem group = new GalleryItem(gallery, SWT.NONE);
				group.setText(information.getHeader());
				group.setExpanded(true);
				group.setFont(infoVaultLogOn.fontGalleryItem);
				for (String aListIndex : listIndex) {
					GalleryItem item = new GalleryItem(group, SWT.NONE);
					Image itemImage = infoVaultDBimg.getImage(information.getHeader(), aListIndex);
					if (itemImage != null) {
						item.setImage(itemImage);
						anImage tmpImage = new anImage();
						tmpImage.imageName = information.getHeader();
						tmpImage.imageIndex = aListIndex;
						tmpImage.imageItem = item;
						Images.add(tmpImage);
					}
				}
			}
		}

		Menu menu = new Menu(gallery);
		gallery.setMenu(menu);
		MenuItem exportItem = new MenuItem(menu, SWT.NONE);
		exportItem.setText("Export image to PNG file");
		exportItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (CurrentGalleryItem == -1) return;
				FileDialog dialog = new FileDialog(infoVaultUI.shell, SWT.SAVE);
				dialog.setText(infoVault.sTitle);
				dialog.setFilterExtensions(new String [] {"*.png"});
				dialog.setFilterNames(new String[] { "PNG image files (*.png)" });
				dialog.setFilterPath(System.getProperty("user.home"));
				String pngfile = dialog.open();
				if (pngfile == null) return;
				anImage tmpImage = Images.get(CurrentGalleryItem);
				ImageLoader loader = new ImageLoader();
				Image itemImage = infoVaultDBimg.getImage(tmpImage.imageName, tmpImage.imageIndex);
				loader.data = new ImageData[] {itemImage.getImageData()};
				loader.save(pngfile, SWT.IMAGE_PNG);
			}
		});

		gallery.addListener(SWT.MenuDetect, event -> {
			Point eventPoint = new Point(event.x, event.y);
			CurrentGalleryItem = -1;	// Will be used in "widgetSelected" above...
			for (int k = 0; k < Images.size(); ++k) {
				anImage tmpImage = Images.get(k);
				Rectangle r = tmpImage.imageItem.getBounds();
				Point absoluteItem = ((Control) event.widget).toDisplay(r.x, r.y);
				r.x = absoluteItem.x;
				r.y = absoluteItem.y;
				if (r.contains(eventPoint)) {
					CurrentGalleryItem = k;
					menu.setLocation(eventPoint);
					menu.setVisible(true);
					return;
				}
				event.doit = false;
			}
		});
	}
	//--------------------------------------------------------------------------
	static void refreshGallery(infoVaultUI.anInformationHeader[] listInformation) {
		Composite parent = gallery.getParent();
		gallery.dispose();
		displayGallery(parent, listInformation);
		gallery.layout();
		gallery.getParent().layout();
	}
	*/
}
//==============================================================================
