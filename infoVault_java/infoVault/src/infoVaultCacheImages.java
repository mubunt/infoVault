//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import java.util.ArrayList;
//------------------------------------------------------------------------------
class infoVaultCacheImages {
	private static final ArrayList<Image> images = new ArrayList<>();
	//--------------------------------------------------------------------------
	static Image get(String s) {
		Image $ = new Image(Display.getDefault(), s);
		images.add($);
		return $;
	}
	//--------------------------------------------------------------------------
	static void disposeAll() {
		for (int k = 0; k < images.size(); ++k) {
			if (images.get(k) == null) continue;
			images.get(k).dispose();
			images.set(k, null);
		}
	}
}
//==============================================================================
