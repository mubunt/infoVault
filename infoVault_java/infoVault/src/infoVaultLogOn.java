///------------------------------------------------------------------------------
// Copyright (c) 2016-2018, 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
//------------------------------------------------------------------------------
class infoVaultLogOn {
	static String encryptionKey;
	static Font fontText, fontColumnName, fontNumbered, fontButton,
			fontLabel, fontCell, fontCellBold, fontCellItalic, fontTable;
	//static Font fontGalleryItem;
	static Image boldImage, italicImage, underlineImage, strikeoutImage,
			foregroundImage, backgroundImage, imagesImage, urlsImage,
			textImage, tableImage, exportImage, importImage, sortImage;
	static Color URLCOLOR, TABLEFOREGROUND, TABLEBACKGROUND,
			TABLECOLUMNTITLEFOREGROUND, TABLECOLUMNTITLEBACKGROUND;
	//static Color GALLERYITEMTITLECOLOR, GALLERYITEMCOLOR_FROM, GALLERYITEMCOLOR_TO;
	static Shell shellLogOn;
	private static String InstallRootDir;
	private static Image splashbackground;
	private static Font fontTitle;

	private static final String TEXT = "/images/32x32_text.png";
	private static final String TABLE = "/images/32x32_table.png";
	private static final String BOLD = "/images/32x32_bold.png";
	private static final String ITALIC = "/images/32x32_italic.png";
	private static final String UNDERLINE = "/images/32x32_underline.png";
	private static final String STRIKEOUT = "/images/32x32_strike.png";
	private static final String FOREGROUND = "/images/32x32_font_color.png";
	private static final String BACKGROUND = "/images/32x32_background.png";
	private static final String IMAGES = "/images/32x32_images.png";
	private static final String URL = "/images/32x32_url.png";
	private static final String EXPORT = "/images/32x32_export.png";
	private static final String IMPORT = "/images/32x32_import.png";
	private static final String SORT = "/images/32x32_sort.png";
	private static final String SPLASHBACKGROUND = "/images/infoVaultBack.jpeg";

	private static final String LABEXIT= "EXIT";
	private static final String LABLICENSE = "License";
	private static final String LABCHANGE = "Change your encryption key";
	private static final String LABOK = "OK";
	private static final String LABKEY = "Your Encryption Key: 0x";

	private static boolean hovering = false;
	//--------------------------------------------------------------------------
	static void LogOn(String root) {
		InstallRootDir = root;
		checkIcons(InstallRootDir);

		Display display = Display.getDefault();

		initFonts();
		initImages();
		initColors();

		infoVaultDB.lockDB(display);
		encryptionShell(display);
		if (encryptionKey != null && encryptionKey.length() == 16)
			infoVaultUI.vaultShell(display);
		infoVaultDB.unlockDB();
	}
	//--------------------------------------------------------------------------
	private static void encryptionShell(Display d) {
		shellLogOn = new Shell(d, SWT.NO_TRIM);
		shellLogOn.setText(infoVault.sDescr);
		shellLogOn.setBackgroundImage(splashbackground);
		shellLogOn.setLayout(new GridLayout());
		shellLogOn.setCursor(d.getSystemCursor(SWT.CURSOR_ARROW));
		//----------------------------------------------------------------------
		Label titKey = new Label(shellLogOn, SWT.NONE);
		titKey.setText(infoVault.sDescr);
		titKey.setFont(fontTitle);
		titKey.setBackground(d.getSystemColor(SWT.COLOR_BLACK));
		titKey.setForeground(d.getSystemColor(SWT.COLOR_WHITE));
		//----------------------------------------------------------------------
		Label labKey = new Label(shellLogOn, SWT.NONE);
		labKey.setText(LABKEY);
		labKey.setFont(fontLabel);
		labKey.setBackground(d.getSystemColor(SWT.COLOR_BLACK));
		labKey.setForeground(d.getSystemColor(SWT.COLOR_WHITE));

		Text txtKey = new Text(shellLogOn, SWT.BORDER | SWT.PASSWORD);
		txtKey.setTextLimit(16);
		txtKey.setFont(fontText);
		txtKey.setBackground(d.getSystemColor(SWT.COLOR_GRAY));
		txtKey.setForeground(d.getSystemColor(SWT.COLOR_BLACK));
		txtKey.forceFocus();
		txtKey.addTraverseListener(e -> {
			if (e.detail == SWT.TRAVERSE_RETURN) {
				// To ignore 'return': e.doit = false; e.detail = SWT.TRAVERSE_NONE;
				if (checkEncryptionKey(txtKey)) return;
				e.doit = false;
			}
		});
		//----------------------------------------------------------------------
		Label exit = new Label(shellLogOn, SWT.NONE);
		exit.setText("   " + LABEXIT);
		exit.setFont(fontButton);
		exit.setBackground(d.getSystemColor(SWT.COLOR_BLACK));
		exit.setForeground(d.getSystemColor(SWT.COLOR_WHITE));
		exit.addMouseListener(new MouseListener() {
			@Override
			public void mouseDown(MouseEvent arg0) {
				infoVaultDB.unlockDB();
				infoVault.Exit(infoVault.TOBEUNLOCKED, 0);
			}
			@Override
			public void mouseUp(MouseEvent arg0) { }
			@Override
			public void mouseDoubleClick(MouseEvent arg0) { }
		});
		Listener mouseHover1 = e -> {
			hovering = e.type == SWT.MouseEnter;
			if (hovering)
				exit.setBackground(d.getSystemColor(SWT.COLOR_GRAY));
			else
				exit.setBackground(d.getSystemColor(SWT.COLOR_BLACK));
		};
		exit.addListener(SWT.MouseEnter, mouseHover1);
		exit.addListener(SWT.MouseExit, mouseHover1);
		//----------------------------------------------------------------------
		Label license = new Label(shellLogOn, SWT.NONE);
		license.setText("   " + LABLICENSE);
		license.setFont(fontButton);
		license.setBackground(d.getSystemColor(SWT.COLOR_BLACK));
		license.setForeground(d.getSystemColor(SWT.COLOR_WHITE));
		license.addMouseListener(new MouseListener() {
			@Override
			public void mouseDown(MouseEvent arg0) { infoVaultAbout.View(); }
			@Override
			public void mouseUp(MouseEvent arg0) { }
			@Override
			public void mouseDoubleClick(MouseEvent arg0) { }
		});
		Listener mouseHover2 = e -> {
			hovering = e.type == SWT.MouseEnter;
			if (hovering)
				license.setBackground(d.getSystemColor(SWT.COLOR_GRAY));
			else
				license.setBackground(d.getSystemColor(SWT.COLOR_BLACK));
		};
		license.addListener(SWT.MouseEnter, mouseHover2);
		license.addListener(SWT.MouseExit, mouseHover2);
		//----------------------------------------------------------------------
		Label change = new Label(shellLogOn, SWT.NONE);
		change.setText("   " + LABCHANGE);
		change.setFont(fontButton);
		change.setBackground(d.getSystemColor(SWT.COLOR_BLACK));
		change.setForeground(d.getSystemColor(SWT.COLOR_WHITE));
		change.addMouseListener(new MouseListener() {
			@Override
			public void mouseDown(MouseEvent arg0) { infoVaultKeyChange.view(Display.getCurrent()); }
			@Override
			public void mouseUp(MouseEvent arg0) { }
			@Override
			public void mouseDoubleClick(MouseEvent arg0) { }
		});
		Listener mouseHover3 = e -> {
			hovering = e.type == SWT.MouseEnter;
			if (hovering)
				change.setBackground(d.getSystemColor(SWT.COLOR_GRAY));
			else
				change.setBackground(d.getSystemColor(SWT.COLOR_BLACK));
		};
		change.addListener(SWT.MouseEnter, mouseHover3);
		change.addListener(SWT.MouseExit, mouseHover3);
		//----------------------------------------------------------------------
		Label ok = new Label(shellLogOn, SWT.BORDER | SWT.CENTER);
		ok.setText(LABOK);
		ok.setFont(fontLabel);
		ok.setBackground(d.getSystemColor(SWT.COLOR_BLACK));
		ok.setForeground(d.getSystemColor(SWT.COLOR_WHITE));
		//ok.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		ok.addMouseListener(new MouseListener() {
			@Override
			public void mouseDown(MouseEvent arg0) {
				if (checkEncryptionKey(txtKey)) return;
			}
			@Override
			public void mouseUp(MouseEvent arg0) { }
			@Override
			public void mouseDoubleClick(MouseEvent arg0) { }
		});
		Listener mouseHover4= e -> {
			hovering = e.type == SWT.MouseEnter;
			if (hovering)
				ok.setBackground(d.getSystemColor(SWT.COLOR_GRAY));
			else
				ok.setBackground(d.getSystemColor(SWT.COLOR_BLACK));
		};
		ok.addListener(SWT.MouseEnter, mouseHover4);
		ok.addListener(SWT.MouseExit, mouseHover4);
		//----------------------------------------------------------------------
		shellLogOn.pack();
		shellLogOn.setSize(600, 260);

		titKey.setLocation(new Point(10,10));
		labKey.setLocation(new Point(230,60));

		txtKey.setBounds(410,55, 140, txtKey.getSize().y);
		exit.setBounds(10,220, exit.getSize().x + 20, exit.getSize().y + 3);
		license.setBounds(10,195, license.getSize().x + 20, license.getSize().y + 3);
		change.setBounds(10,170, change.getSize().x + 20, change.getSize().y + 3);
		ok.setBounds(450, 220, ok.getSize().x + 80, ok.getSize().y);
		//----------------------------------------------------------------------
		Rectangle shellRect = shellLogOn.getBounds();
		Rectangle displayRect = d.getBounds();
		shellLogOn.setLocation((displayRect.width - shellRect.width) / 2, (displayRect.height - shellRect.height) / 2);
		//----------------------------------------------------------------------
		shellLogOn.open();
		while (!shellLogOn.isDisposed())
			if (!d.readAndDispatch())
				d.sleep();
	}
	//==========================================================================
	private static boolean checkEncryptionKey(Text txtKey) {
		encryptionKey = infoVaultAES.encryptionKey(txtKey.getText());
		try {
			infoVaultDB.testEncryptionKey();
		} catch (SQLException e) {
			displayError(shellLogOn, "SQL error ('test encryption key' exception): " + e.getMessage());
			infoVault.Exit(true, -1);
		} catch (Exception e) {
			displayError(shellLogOn, "Bad encryption key. Retry please.");
			txtKey.clearSelection();
			txtKey.setText("");
			txtKey.forceFocus();
			return false;
		}
		shellLogOn.dispose();
		return true;
	}
	//--------------------------------------------------------------------------
	private static void checkIcons(String root) {
		java.util.List<String> images = new ArrayList<>();
		images.add(BOLD);
		images.add(ITALIC);
		images.add(UNDERLINE);
		images.add(STRIKEOUT);
		images.add(FOREGROUND);
		images.add(BACKGROUND);
		images.add(IMAGES);
		images.add(URL);
		images.add(EXPORT);
		images.add(IMPORT);
		images.add(SORT);

		for (String file : images) {
			if (!(new File(root + file)).exists()) {
				System.err.println(infoVault.sError + "File " + root + file + " does not exist. Incorrect installation!");
				System.exit(-1);
			}
		}
	}
	//--------------------------------------------------------------------------
	private static void displayError(Shell shell, String error) {
		MessageBox messageBox = new MessageBox(shell, SWT.OK | SWT.ICON_ERROR);
		messageBox.setMessage(error);
		messageBox.open();
	}
	//==========================================================================
	private static void initFonts() {
		fontButton = infoVaultCacheFonts.add("Arial", 10, SWT.BOLD);
		fontLabel = infoVaultCacheFonts.add("Arial", 11, SWT.BOLD);
		fontText = infoVaultCacheFonts.add("Arial", 10, SWT.NORMAL);
		fontTitle = infoVaultCacheFonts.add("Arial", 14, SWT.BOLD | SWT.ITALIC);
		fontColumnName = infoVaultCacheFonts.add("Arial", 10, SWT.BOLD);
		fontCell = infoVaultCacheFonts.add("Arial", 9, SWT.NORMAL);
		fontCellBold = infoVaultCacheFonts.add("Arial", 9, SWT.BOLD);
		fontCellItalic = infoVaultCacheFonts.add("Arial", 9, SWT.ITALIC);
		fontNumbered = infoVaultCacheFonts.add("Arial", 9, SWT.BOLD);
		//fontGalleryItem = infoVaultCacheFonts.add("Arial", 10, SWT.NORMAL);
		fontTable = infoVaultCacheFonts.add("Arial", 9, SWT.NORMAL);
	}
	private static void initImages() {
		splashbackground = infoVaultCacheImages.get(InstallRootDir + SPLASHBACKGROUND);
		boldImage = infoVaultCacheImages.get(InstallRootDir + BOLD);
		italicImage = infoVaultCacheImages.get(InstallRootDir + ITALIC);
		underlineImage = infoVaultCacheImages.get(InstallRootDir + UNDERLINE);
		strikeoutImage = infoVaultCacheImages.get(InstallRootDir + STRIKEOUT);
		foregroundImage = infoVaultCacheImages.get(InstallRootDir + FOREGROUND);
		backgroundImage = infoVaultCacheImages.get(InstallRootDir + BACKGROUND);
		imagesImage = infoVaultCacheImages.get(InstallRootDir + IMAGES);
		urlsImage = infoVaultCacheImages.get(InstallRootDir + URL);
		textImage = infoVaultCacheImages.get(InstallRootDir + TEXT);
		tableImage = infoVaultCacheImages.get(InstallRootDir + TABLE);
		exportImage = infoVaultCacheImages.get(InstallRootDir + EXPORT);
		importImage = infoVaultCacheImages.get(InstallRootDir + IMPORT);
		sortImage = infoVaultCacheImages.get(InstallRootDir + SORT);
	}
	private static void initColors() {
		TABLEFOREGROUND = infoVaultCacheColors.get(0, 0, 0);
		TABLEBACKGROUND = infoVaultCacheColors.get(255, 255, 255);
		TABLECOLUMNTITLEFOREGROUND = infoVaultCacheColors.get(255, 255, 255);	// COLOR_TITLE_FOREGROUND
		TABLECOLUMNTITLEBACKGROUND = infoVaultCacheColors.get(82, 148, 226);	// COLOR_TITLE_BACKGROUND
		//GALLERYITEMCOLOR_TO = infoVaultCacheColors.get(242, 242, 242);
		//GALLERYITEMCOLOR_FROM = infoVaultCacheColors.get(204, 204, 204);
		//GALLERYITEMTITLECOLOR = infoVaultCacheColors.get(0, 0, 0);
		URLCOLOR = infoVaultCacheColors.get(166, 172, 179);					// COLOR_WIDGET_NORMAL_SHADOW
	}
	//==========================================================================
	static void Exit() {
		infoVaultCacheFonts.disposeAll();
		infoVaultCacheImages.disposeAll();
		infoVaultCacheColors.disposeAll();
	}
}
//==============================================================================
