//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
//------------------------------------------------------------------------------
class infoVaultAbout {
	private static final String eol = System.getProperty("line.separator");
	//--------------------------------------------------------------------------
	static void View() {
		Font fontAbout = infoVaultCacheFonts.add("Arial", 10, SWT.NORMAL);
		Font fontTool = infoVaultCacheFonts.add("Arial", 14, SWT.BOLD);

		Shell shell = new Shell(Display.getCurrent(), SWT.CLOSE | SWT.TITLE);
		shell.setLayout(new FillLayout());
		shell.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		shell.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		shell.setBackgroundMode(SWT.INHERIT_DEFAULT);
		shell.setText(infoVault.sAboutTitle);

		StyledText aboutWin = new StyledText(shell, SWT.WRAP | SWT.BORDER);
		aboutWin.setFont(fontAbout);
		aboutWin.setEditable(false);
		aboutWin.setText(eol + infoVault.toolName + eol + eol
				+ "Version " + infoVaultBuildInfo.getVersion() + " - " +
				"Build " + infoVaultBuildInfo.getNumber() + ", " +
				infoVaultBuildInfo.getDate() + eol + eol
				+ infoVault.sAboutTool);
		aboutWin.setAlignment(SWT.CENTER);
		aboutWin.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));

		StyleRange[] styles = new StyleRange[1];
		styles[0] = new StyleRange();
		styles[0].font = fontTool;
		int[] ranges = new int[]{1, infoVault.toolName.length() + 1};
		aboutWin.setStyleRanges(ranges, styles);

		shell.setSize(650, 340);
		Rectangle shellRect = shell.getBounds();
		Rectangle displayRect = Display.getCurrent().getBounds();
		shell.setLocation((displayRect.width - shellRect.width) / 2, (displayRect.height - shellRect.height) / 2);
		shell.open();
		while (!shell.isDisposed())
			if (!Display.getCurrent().readAndDispatch())
				Display.getCurrent().sleep();
	}
}
//==========================================================================
