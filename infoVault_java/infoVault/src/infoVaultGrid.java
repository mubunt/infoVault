//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.apache.commons.lang3.StringUtils;
import org.eclipse.nebula.widgets.grid.Grid;
import org.eclipse.nebula.widgets.grid.GridColumn;
import org.eclipse.nebula.widgets.grid.GridEditor;
import org.eclipse.nebula.widgets.grid.GridItem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.*;

import java.io.*;
//------------------------------------------------------------------------------
class infoVaultGrid {
	//--------------------------------------------------------------------------
	private static class lastSelection {
		int row;
		int col;
	}
	private static lastSelection lastSel;
	private static final int ROWCOUNT = 20;
	private static final int COLUMNCOUNT = 5;
	private static final int DEFAULTWIDTH = 150;
	private static final String prefixURLforTABLE = "[ ";
	private static final String suffixURLforTABLE = " ]";
	//--------------------------------------------------------------------------
	static Grid gridArea(Composite parent, int idx) {
		String name = infoVaultInformation.getInformationName(idx);

		final GridData gd = new GridData();
		gd.grabExcessHorizontalSpace = true;
		gd.horizontalAlignment = GridData.FILL;
		gd.grabExcessVerticalSpace = true;
		gd.verticalAlignment = GridData.FILL;

		Grid $ = new Grid(parent, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		$.setLayoutData(gd);
		$.setAutoHeight(true);
		$.setHeaderVisible(true);
		$.setCellSelectionEnabled(true);
		$.setCellHeaderSelectionBackground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_YELLOW));
		if (infoVaultInformation.doesInformationExist(idx))
			createGrid($, infoVaultDB.getContent(name), infoVaultDB.getStyle(name));
		else
			createDefaultGrid($);
		setEditorColumnName($);
		addPopupMenu($);
		addEditionCapabilities($);
		lastSel = new lastSelection();
		lastSel.row = lastSel.col = -1;
		return $;
	}
	//--------------------------------------------------------------------------
	private static void setEditorColumnName(Grid grid) {
		for (int i = 0; i < grid.getColumnCount(); ++i) {
			grid.getColumn(i).addSelectionListener(new SelectionListener() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					int icolumn = ((GridColumn) e.item).getCellRenderer().getColumn();
					GridColumn column = grid.getColumn(icolumn);
					GridItem item = grid.getItem(0);
					Text text = new Text(grid, SWT.NONE);
					text.setText(column.getText());
					text.setFocus();
					String initialValue = text.getText();
					final GridEditor editor = new GridEditor(grid);
					editor.grabHorizontal = true;
					editor.grabVertical = true;
					editor.setEditor(text, item, icolumn);
					// Handler to detect key presses
					text.addKeyListener(new KeyAdapter() {
						public void keyPressed(KeyEvent event) {
							// End the editing and save the text if the user presses Enter
							// End the editing and throw away the text if the user presses Esc
							switch (event.keyCode) {
								case SWT.CR:
									column.setText(text.getText());
									if (!initialValue.equals(column.getText()))
										infoVaultUI.markInformationAsModified(infoVaultUI.getCurrentPShelf());
									text.dispose();
									break;
								case SWT.ESC:
									text.dispose();
									break;
							}
						}
					});
					// close the text editor and copy the data over when the user clicks away
					FocusListener focuslistener = new FocusListener() {
						@Override
						public void focusGained(FocusEvent event) {
						}
						@Override
						public void focusLost(FocusEvent event) {
							column.setText(text.getText());
							if (!initialValue.equals(column.getText()))
								infoVaultUI.markInformationAsModified(infoVaultUI.getCurrentPShelf());
							text.dispose();
						}
					};
					text.addFocusListener(focuslistener);
				}
				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		}
	}
	//--------------------------------------------------------------------------
	private static void createGrid(Grid grid, String content, String presentation) {
		// --- Columns
		String[] columns = (content.substring(
				content.indexOf(infoVaultInformation. B_TABLECOLUMN) + infoVaultInformation.B_TABLECOLUMN.length(),
				content.indexOf(infoVaultInformation.E_TABLECOLUMN)))
				.split(infoVaultInformation.VALUE_SEPARATOR);
		String[] widths = (presentation.substring(
				presentation.indexOf(infoVaultInformation. B_COLUMNSIZE) + infoVaultInformation.B_COLUMNSIZE.length(),
				presentation.indexOf(infoVaultInformation.E_COLUMNSIZE)))
				.split(infoVaultInformation.VALUE_SEPARATOR);
		String[] backgrounds = presentation.contains(infoVaultInformation. B_CELLBACKGROUND) ?
				(presentation.substring(
						presentation.indexOf(infoVaultInformation. B_CELLBACKGROUND) + infoVaultInformation.B_CELLBACKGROUND.length(),
						presentation.indexOf(infoVaultInformation.E_CELLBACKGROUND)))
						.split(infoVaultInformation.VALUE_SEPARATOR)
				: null;
		String[] foregrounds = presentation.contains(infoVaultInformation. B_CELLFOREGROUND) ?
				(presentation.substring(
						presentation.indexOf(infoVaultInformation. B_CELLFOREGROUND) + infoVaultInformation.B_CELLFOREGROUND.length(),
						presentation.indexOf(infoVaultInformation.E_CELLFOREGROUND)))
						.split(infoVaultInformation.VALUE_SEPARATOR)
				: null;
		String[] fonts = presentation.contains(infoVaultInformation. B_CELLFONT) ?
				(presentation.substring(
						presentation.indexOf(infoVaultInformation. B_CELLFONT) + infoVaultInformation.B_CELLFONT.length(),
						presentation.indexOf(infoVaultInformation.E_CELLFONT)))
						.split(infoVaultInformation.VALUE_SEPARATOR)
				: null;
		// --- Columns
		for (int i = 0; i < columns.length; ++i) {
			GridColumn column = new GridColumn(grid, SWT.LEFT);
			String s = StringUtils.stripEnd(columns[i], "\"");
			column.setText(StringUtils.stripStart(s, "\""));
			column.setWidth(Integer.parseInt(widths[i]));
			column.setWordWrap(true);
		}
		// --- Rows
		String rows = content.substring(content.indexOf(infoVaultInformation. E_TABLECOLUMN) + infoVaultInformation.E_TABLECOLUMN.length());
		while (rows.length() != 0) {
			String[] data = (rows.substring(
					infoVaultInformation.B_TABLEITEM.length(),
					rows.indexOf(infoVaultInformation.E_TABLEITEM)))
					.split(infoVaultInformation.VALUE_SEPARATOR);
			GridItem item = new GridItem(grid, SWT.NONE);
			item.setFont(infoVaultLogOn.fontCell);
			for (int i = 0; i < data.length; ++i) {
				item.setBackground(i, infoVaultLogOn.TABLEBACKGROUND);
				item.setForeground(i, infoVaultLogOn.TABLEFOREGROUND);
				item.setFont(i, infoVaultLogOn.fontTable);
				item.setText(i, data[i].substring(1, data[i].length() - 1));
			}
			rows = rows.substring(rows.indexOf(infoVaultInformation.E_TABLEITEM) + infoVaultInformation.E_TABLEITEM.length());
		}
		//GridItem item = grid.getItem(0);
		//item.setFont(infoVaultLogOn.fontColumnName);
		//item.setBackground(infoVaultLogOn.TABLECOLUMNTITLEBACKGROUND);
		//item.setForeground(infoVaultLogOn.TABLECOLUMNTITLEFOREGROUND);
		// Background
		if (backgrounds != null) {    // backward compatibility
			for (String s : backgrounds) {
				if (s.length() != 0) {
					String[] ss = s.split("/");
					if (ss.length != 3) {
						infoVaultUI.displayError("Abnormal situation: Parameters of background color was wrongly saved.", 2);
						continue;
					}
					GridItem row = grid.getItem(Integer.parseInt(ss[0]));
					row.setBackground(Integer.parseInt(ss[1]), infoVaultDB.deserializationColorString(ss[2]));
				}
			}
		}
		// Foreground
		if (foregrounds != null) {    // backward compatibility
			for (String s : foregrounds) {
				if (s.length() != 0) {
					String[] ss = s.split("/");
					if (ss.length != 3) {
						infoVaultUI.displayError("Abnormal situation: Parameters of foreground color was wrongly saved.", 2);
						continue;
					}
					GridItem row = grid.getItem(Integer.parseInt(ss[0]));
					row.setForeground(Integer.parseInt(ss[1]), infoVaultDB.deserializationColorString(ss[2]));
				}
			}
		}
		// Fonts
		if (fonts != null) {    // backward compatibility
			for (String s : fonts) {
				String[] ss = s.split("/");
				if (ss.length != 3) {
					infoVaultUI.displayError("Abnormal situation: Parameters of font was wrongly saved.", 2);
					continue;
				}
				GridItem row = grid.getItem(Integer.parseInt(ss[0]));
				row.setFont(Integer.parseInt(ss[1]), infoVaultDB.deserializationFontString(ss[2]));
			}
		}
		// No table tool tip
		grid.setToolTipText("");
	}
	//--------------------------------------------------------------------------
	private static void createDefaultGrid(Grid grid) {
		// --- Columns
		for (int i = 0; i < COLUMNCOUNT; ++i) {
			GridColumn column = new GridColumn(grid, SWT.LEFT);
			column.setText("Column " + i);
			column.setWidth(DEFAULTWIDTH);
			column.setWordWrap(true);
			if (i == 0) column.setSort(SWT.UP);
		}
		for (int i = 0; i < ROWCOUNT; ++i) {
			GridItem item = new GridItem(grid, SWT.NONE);
			fillEmptyItem(item, grid.getColumnCount());
		}
		// No table tool tip
		grid.setToolTipText("");
	}
	//--------------------------------------------------------------------------
	private static void addPopupMenu(Grid grid) {
		Menu popupMenu = new Menu(grid);
		grid.setMenu(popupMenu);

		MenuItem addLine = new MenuItem(popupMenu, SWT.NONE);
		addLine.setText("Add line below");
		addLine.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int row = grid.getSelectionIndex();
				if (row == -1) return;
				GridItem item = new GridItem(grid, SWT.NONE, row + 1);
				fillEmptyItem(item, grid.getColumnCount());
				infoVaultUI.markInformationAsModified(infoVaultUI.getCurrentPShelf());
			}
		});

		MenuItem deleteLine = new MenuItem(popupMenu, SWT.NONE);
		deleteLine.setText("Delete this/these line(s)");
		deleteLine.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				grid.remove(grid.getSelectionIndices());
				infoVaultUI.markInformationAsModified(infoVaultUI.getCurrentPShelf());
			}
		});

		new MenuItem(popupMenu, SWT.SEPARATOR);

		MenuItem addColumn = new MenuItem(popupMenu, SWT.NONE);
		addColumn.setText("Add column on right");
		addColumn.addListener(SWT.Selection, e -> {
			int xcol = infoVaultUI.shell.getDisplay().getCursorLocation().x - infoVaultUI.shell.getLocation().x;
			int row = grid.getSelectionIndex();
			if (row == -1) return;
			GridItem item = grid.getItem(row);
			for (int col = 0; col < grid.getColumnCount(); ++col) {
				Rectangle rect = item.getBounds(col);
				if (xcol < rect.x || xcol > (rect.x + rect.width)) continue;
				GridColumn column = new GridColumn (grid, SWT.NONE, col + 1);
				column.setWidth(DEFAULTWIDTH);
				column.setWordWrap(true);
				//column.pack();
				infoVaultUI.markInformationAsModified(infoVaultUI.getCurrentPShelf());
				break;
			}
		});

		MenuItem deleteColumn = new MenuItem(popupMenu, SWT.NONE);
		deleteColumn.setText("Delete this column");
		deleteColumn.addListener(SWT.Selection, e -> {
			// 40 = arbitrary value for the difference between "shell" and "grid" !!!!!!
			int xcol = infoVaultUI.shell.getDisplay().getCursorLocation().x - infoVaultUI.shell.getLocation().x - 40;
			int row = grid.getSelectionIndex();
			if (row == -1) return;
			GridItem item = grid.getItem(row);
			for (int col = 0; col < grid.getColumnCount(); ++col) {
				Rectangle rect = item.getBounds(col);
				if (xcol < rect.x || xcol > (rect.x + rect.width)) continue;
				grid.getColumn(col).dispose();
				infoVaultUI.markInformationAsModified(infoVaultUI.getCurrentPShelf());
				break;
			}
		});
	}
	//--------------------------------------------------------------------------
	private static void fillEmptyItem(GridItem item, int colNumber) {
		item.setFont(infoVaultLogOn.fontCell);
		for (int k = 0; k < colNumber; ++k) {
			item.setBackground(k, infoVaultLogOn.TABLEBACKGROUND);
			item.setForeground(k, infoVaultLogOn.TABLEFOREGROUND);
			item.setFont(k, infoVaultLogOn.fontTable);
			item.setText(k, "");
		}
	}
	//--------------------------------------------------------------------------
	private static void addEditionCapabilities(Grid grid) {
		Listener tableListener = new Listener() {
			Shell tip = null;
			Label label = null;
			public void handleEvent(Event e) {
				switch (e.type) {
					case SWT.Dispose:
					case SWT.KeyDown:
					case SWT.MouseMove: {
						if (tip == null) break;
						tip.dispose();
						tip = null;
						label = null;
						break;
					}
					case SWT.MouseHover: {
						Point pt = new Point(e.x, e.y);
						GridItem item = grid.getItem(pt);
						if (item == null) return;
						int row = -1;
						int col;
						for (col = 0; col < grid.getColumnCount(); col++) {
							Rectangle rect = item.getBounds(col);
							if (!rect.contains(pt)) continue;
							row = grid.indexOf(item);
							break;
						}
						if (row == -1 || col < 0) return;
						String s = item.getText(col);
						if (! s.startsWith(prefixURLforTABLE) || ! s.endsWith(suffixURLforTABLE)) return;
						if (tip != null && !tip.isDisposed()) tip.dispose();
						tip = new Shell(Display.getDefault().getActiveShell(), SWT.ON_TOP | SWT.TOOL);
						tip.setLayout(new FillLayout());
						label = new Label(tip, SWT.NONE);
						label.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_WHITE));
						label.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_BLACK));
						label.setText("  Hyperlink to " + s.replace(prefixURLforTABLE, "").replace(suffixURLforTABLE, "") + "  ");
						Point size = tip.computeSize(SWT.DEFAULT, SWT.DEFAULT);
						Rectangle rect = item.getBounds(col);
						pt = grid.toDisplay(rect.x, rect.y);
						tip.setBounds(pt.x, pt.y, size.x, size.y);
						tip.setVisible(true);
					}
				}
			}
		};

		grid.addListener(SWT.Dispose, tableListener);
		grid.addListener(SWT.KeyDown, tableListener);
		grid.addListener(SWT.MouseMove, tableListener);
		grid.addListener(SWT.MouseHover, tableListener);

		grid.addMouseListener(new MouseListener() {
			private boolean doubleClick;
			public void mouseUp(MouseEvent e) { }
			public void mouseDown(MouseEvent e) {
				doubleClick = false;
				Display.getDefault().timerExec(Display.getDefault().getDoubleClickTime(), () -> {
					if (!doubleClick) {
						// Select the row and column in the table
						Point pt = new Point(e.x, e.y);
						GridItem item = grid.getItem(pt);
						if (item == null) return;
						for (int col = 0; col < grid.getColumnCount(); col++) {
							Rectangle rect = item.getBounds(col);
							if (! rect.contains(pt)) continue;
							lastSel.row = grid.indexOf(item);
							lastSel.col = col;
							break;
						}
						if ((e.stateMask & SWT.MOD1) != 0) {
							String s = item.getText(lastSel.col);
							if (s.startsWith(prefixURLforTABLE) && s.endsWith(suffixURLforTABLE)) {
								// Follow url on CTRL / Mouse Click
								Program.launch(s.replace(prefixURLforTABLE, "").replace(suffixURLforTABLE, ""));
							}
						}
					}
				});
			}
			public void mouseDoubleClick(MouseEvent e) {
				doubleClick = true;
				// Select the row and column in the table
				Point pt = new Point(e.x, e.y);
				GridItem item = grid.getItem(pt);
				if (item == null) return;
				lastSel.row = lastSel.col = -1;
				for (int col = 0; col < grid.getColumnCount(); col++) {
					Rectangle rect = item.getBounds(col);
					if (!rect.contains(pt)) continue;
					lastSel.row = grid.indexOf(item);
					lastSel.col = col;
					break;
				}
				if (lastSel.row == -1 || lastSel.col < 0) return;
				// Begin an editing session
				Text text = new Text(grid, SWT.NONE);
				text.setText(item.getText(lastSel.col));
				text.setFont(infoVaultLogOn.fontTable);
				text.setFocus();
				String initialValue = text.getText();
				final GridEditor editor = new GridEditor(grid);
				editor.grabHorizontal = true;
				editor.grabVertical = true;
				editor.setEditor(text, item, lastSel.col);
				// Handler to detect key presses
				text.addKeyListener(new KeyAdapter() {
					public void keyPressed(KeyEvent event) {
						// End the editing and save the text if the user presses Enter
						// End the editing and throw away the text if the user presses Esc
						switch (event.keyCode) {
							case SWT.CR:
								item.setText(lastSel.col, text.getText());
								item.setFont(infoVaultLogOn.fontTable);
								if (!initialValue.equals(item.getText(lastSel.col)))
									infoVaultUI.markInformationAsModified(infoVaultUI.getCurrentPShelf());
								text.dispose();
								break;
							case SWT.ESC:
								text.dispose();
								break;
						}
					}
				});
				// close the text editor and copy the data over when the user clicks away
				FocusListener focuslistener = new FocusListener() {
					@Override
					public void focusGained(FocusEvent event) {}
					@Override
					public void focusLost(FocusEvent event) {
						item.setText(lastSel.col, text.getText());
						if (!initialValue.equals(item.getText(lastSel.col)))
							infoVaultUI.markInformationAsModified(infoVaultUI.getCurrentPShelf());
						text.dispose();
					}
				};
				text.addFocusListener(focuslistener);
			}
		});
	}
	//--------------------------------------------------------------------------
	static final SelectionAdapter listenerStyleBold = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			if (lastSel.col == -1) return;
			Grid grid = infoVaultInformation.getInformationGrid(infoVaultUI.getCurrentPShelf());
			int[] ind = grid.getSelectionIndices();
			for (int i  = 0; i < ind.length; i++) {
				GridItem item = grid.getItem(ind[i]);
				Font old = item.getFont(lastSel.col);
				if (old.equals(infoVaultLogOn.fontCellBold))
					item.setFont(lastSel.col, infoVaultLogOn.fontCell);
				else
					item.setFont(lastSel.col, infoVaultLogOn.fontCellBold);
			}
			infoVaultUI.markInformationAsModified(infoVaultUI.getCurrentPShelf());
		}
	};

	static final SelectionAdapter listenerStyleItalic = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			if (lastSel.col == -1) return;
			Grid grid = infoVaultInformation.getInformationGrid(infoVaultUI.getCurrentPShelf());
			int[] ind = grid.getSelectionIndices();
			for (int i  = 0; i < ind.length; i++) {
				GridItem item = grid.getItem(ind[i]);
				Font old = item.getFont(lastSel.col);
				if (old.equals(infoVaultLogOn.fontCellItalic))
					item.setFont(lastSel.col, infoVaultLogOn.fontCell);
				else
					item.setFont(lastSel.col, infoVaultLogOn.fontCellItalic);
			}
			infoVaultUI.markInformationAsModified(infoVaultUI.getCurrentPShelf());
		}
	};

	static final SelectionAdapter listenerColorForeGround = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			if (lastSel.col == -1) return;
			ColorDialog cd = new ColorDialog(infoVaultUI.shell);
			cd.setText(infoVault.sTitle);
			cd.setRGB(new RGB(255, 255, 255));
			RGB newColor = cd.open();
			if (newColor == null) return;
			Grid grid = infoVaultInformation.getInformationGrid(infoVaultUI.getCurrentPShelf());
			int[] ind = grid.getSelectionIndices();
			for (int i  = 0; i < ind.length; i++) {
				GridItem item = grid.getItem(ind[i]);
				item.setForeground(lastSel.col, infoVaultCacheColors.get(newColor));
			}
			infoVaultUI.markInformationAsModified(infoVaultUI.getCurrentPShelf());
		}
	};

	static final SelectionAdapter listenerColorBackGround = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			if (lastSel.col == -1) return;
			ColorDialog cd = new ColorDialog(infoVaultUI.shell);
			cd.setText(infoVault.sTitle);
			cd.setRGB(new RGB(255, 255, 255));
			RGB newColor = cd.open();
			if (newColor == null) return;
			Grid grid = infoVaultInformation.getInformationGrid(infoVaultUI.getCurrentPShelf());
			int[] ind = grid.getSelectionIndices();
			for (int i  = 0; i < ind.length; i++) {
				GridItem item = grid.getItem(ind[i]);
				item.setBackground(lastSel.col, infoVaultCacheColors.get(newColor));
			}
			infoVaultUI.markInformationAsModified(infoVaultUI.getCurrentPShelf());
		}
	};

	static final SelectionAdapter listenerUrl = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			if (lastSel.col == -1) return;
			Grid grid = infoVaultInformation.getInformationGrid(infoVaultUI.getCurrentPShelf());
			int[] ind = grid.getSelectionIndices();
			for (int i  = 0; i < ind.length; i++) {
				GridItem item = grid.getItem(ind[i]);
				String s = item.getText(lastSel.col);
				if (s.startsWith(prefixURLforTABLE) && s.endsWith(suffixURLforTABLE)) {
					item.setText(lastSel.col, s.replace(prefixURLforTABLE, "").replace(suffixURLforTABLE, ""));
					item.setForeground(lastSel.col, infoVaultLogOn.TABLEFOREGROUND);
				} else {
					String ss = prefixURLforTABLE + s + suffixURLforTABLE;
					item.setText(lastSel.col, ss);
					item.setForeground(lastSel.col, infoVaultLogOn.URLCOLOR);
				}
				item.setFont(lastSel.col, infoVaultLogOn.fontTable);
			}
			infoVaultUI.markInformationAsModified(infoVaultUI.getCurrentPShelf());
		}
	};

	static final SelectionAdapter listenerExport = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			Grid grid = infoVaultInformation.getInformationGrid(infoVaultUI.getCurrentPShelf());
			if (grid.getColumnCount() == 0) return;
			String content = infoVaultInformation.getInformationGridContentCSV(infoVaultUI.getCurrentPShelf());
			FileDialog fd = new FileDialog(infoVaultUI.shell, SWT.SAVE);
			fd.setText("Select the file in which ALL the data will be exported:");
			String[] filterExt = { "*.csv", "*.*" };
			String[] filterNames = { "CSV files", "All files" };
			fd.setFilterExtensions(filterExt);
			fd.setFilterNames(filterNames);
			String selected = fd.open();
			if (selected == null) return;
			BufferedWriter writer = null;
			try {
				writer = new BufferedWriter(new FileWriter(selected));
				writer.write(content);
			} catch ( IOException e1) { e1.printStackTrace(); }
			finally {
				if (writer != null) {
					try { writer.close( ); } catch (IOException e2) { e2.printStackTrace(); }
				}
			}
		}
	};

	static final SelectionAdapter listenerImport = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			FileDialog fd = new FileDialog(infoVaultUI.shell, SWT.OPEN);
			fd.setText("Select the file from which the data will be imported:");
			String[] filterExt = {"*.csv", "*.*"};
			String[] filterNames = {"CSV files", "All files"};
			fd.setFilterExtensions(filterExt);
			fd.setFilterNames(filterNames);
			String selected = fd.open();
			if (selected == null) return;
			Grid grid = infoVaultInformation.getInformationGrid(infoVaultUI.getCurrentPShelf());
			BufferedReader reader;
			try {
				reader = new BufferedReader(new FileReader(selected));
				String line = reader.readLine();
				while (line != null) {
					String[] data = line.split(",");
					GridItem item = new GridItem(grid, SWT.NONE);
					item.setFont(infoVaultLogOn.fontCell);
					for (int i = 0; i < data.length; ++i) {
						item.setText(i, data[i]);
						if (data[i].startsWith(prefixURLforTABLE) && data[i].endsWith(suffixURLforTABLE))
							item.setForeground(i, infoVaultLogOn.URLCOLOR);
						else
							item.setForeground(i, infoVaultLogOn.TABLEFOREGROUND);
						item.setBackground(i, infoVaultLogOn.TABLEBACKGROUND);
						item.setFont(i, infoVaultLogOn.fontTable);
					}
					line = reader.readLine();
				}
				reader.close();
			} catch (IOException e1) { e1.printStackTrace(); }
			infoVaultUI.markInformationAsModified(infoVaultUI.getCurrentPShelf());
		}
	};
	//--------------------------------------------------------------------------
	static final SelectionAdapter listenerSort = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			Grid grid = infoVaultInformation.getInformationGrid(infoVaultUI.getCurrentPShelf());
			if (grid.getColumnCount() == 0) return;
			int[] index = new int[grid.getItemCount()];
			GridItem[] values = grid.getItems();
			quicksort(values, grid.getColumnCount(), 0, index.length - 1);
			infoVaultUI.markInformationAsModified(infoVaultUI.getCurrentPShelf());
		}
	};
	private static void quicksort(GridItem[] a, int nbcols, int left, int right) {
		if (right <= left) return;
		int i = partition(a, nbcols, left, right);
		quicksort(a, nbcols, left, i-1);
		quicksort(a, nbcols, i+1, right);
	}
	private static int mid(GridItem[] a,int left, int right) {
		int i = left;
		int j = (left + right) / 2;
		// order the first two
		int cmp = cmp(a, i, j);
		if (cmp > 0) {
			int tmp = j;
			j = i;
			i = tmp;
		}
		cmp = cmp(a, j, right);
		if (cmp > 0) {
			cmp = cmp(a, i, right);
			if (cmp > 0) return i;
			return right;
		}
		return j;
	}
	private static int partition(GridItem[] a, int nbcols, int left, int right) {
		int mid = mid(a, left, right);
		swap(a, nbcols, right, mid);
		int i = left - 1;
		int j = right;
		while (true) {
			while (cmp(a, ++i, right) < 0) ;
			while (cmp(a, right, --j) < 0) if (j == left) break;
			if (i >= j) break;
			swap(a, nbcols, i, j);
		}
		swap(a, nbcols, i, right);
		return i;
	}
	private static void swap(GridItem[] a, int nbcols, int aindex, int bindex) {
		for (int i = 0; i < nbcols; i++) {
			String tmpText = a[aindex].getText(i);
			a[aindex].setText(i, a[bindex].getText(i));
			a[bindex].setText(i, tmpText);

			Color tmpBack = a[aindex].getBackground(i);
			a[aindex].setBackground(i, a[bindex].getBackground(i));
			a[bindex].setBackground(i, tmpBack);

			Color tmpFore = a[aindex].getForeground(i);
			a[aindex].setForeground(i, a[bindex].getForeground(i));
			a[bindex].setForeground(i, tmpFore);

			Font tmpFont = a[aindex].getFont(i);
			a[aindex].setFont(i, a[bindex].getFont(i));
			a[bindex].setFont(i, tmpFont);
		}
	}
	private static int cmp(GridItem[] a, int aindex, int bindex) {
		return((a[aindex].getText(0)).compareTo(a[bindex].getText(0)));
	}
}
//==============================================================================
