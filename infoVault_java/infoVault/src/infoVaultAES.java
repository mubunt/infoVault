//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//
// Original sources from the WEB. Author (s) unknown.
//------------------------------------------------------------------------------
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.concurrent.atomic.AtomicReference;
//------------------------------------------------------------------------------
class infoVaultAES {
	//--------------------------------------------------------------------------
	static String encrypt(String key, String message) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
		return byteArrayToHex(cipher.doFinal(message.getBytes()));
	}
	static String encrypt(String key, int value) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
		return byteArrayToHex(cipher.doFinal(String.valueOf(value).getBytes()));
	}
	static byte[] encrypt(String key, byte[] Data) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
		return cipher.doFinal(Data);
	}
	//--------------------------------------------------------------------------
	static String decrypt(String key, String encrypted) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		cipher.init(Cipher.DECRYPT_MODE, skeySpec);
		byte[] original = cipher.doFinal(hexToByteArray(encrypted));
		return new String(original);
	}
	static int decrypti(String key, String encrypted) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		cipher.init(Cipher.DECRYPT_MODE, skeySpec);
		byte[] original = cipher.doFinal(hexToByteArray(encrypted));
		return Integer.parseInt(new String(original));
	}
	static byte[] decrypt(String key, byte[] encryptedData) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		cipher.init(Cipher.DECRYPT_MODE, skeySpec);
		return cipher.doFinal(encryptedData);
	}
	//--------------------------------------------------------------------------
	static String encryptionKey(String key) {
		return !key.matches("\\p{XDigit}+") ? "" : ("0000000000000000" + key).substring(key.length());
	}
	//--------------------------------------------------------------------------
	private static byte[] hexToByteArray(String hex) {
		if (hex == null || hex.length() == 0)
			return null;
		byte[] $ = new byte[hex.length() / 2];
		for (int i = 0; i < $.length; ++i)
			$[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
		return $;
	}
	//--------------------------------------------------------------------------
	private static String byteArrayToHex(byte[] ba) {
		if (ba == null || ba.length == 0)
			return null;
		AtomicReference<StringBuilder> sb = new AtomicReference<>(new StringBuilder(2 * ba.length));
		String hexNumber;
		//noinspection ForLoopReplaceableByForEach
		for (int i = 0, baLength = ba.length; i < baLength; i++) {
			byte aBa = ba[i];
			hexNumber = "0" + Integer.toHexString(aBa & 0xff);
			sb.get().append(hexNumber.substring(hexNumber.length() - 2));
		}
		return sb.get() + "";
	}
}
//==============================================================================