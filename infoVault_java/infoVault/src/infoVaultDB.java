//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import java.io.File;
import java.io.IOException;
import java.sql.*;
//------------------------------------------------------------------------------
class infoVaultDB {
	private static final String homeDirectory = System.getProperty("user.home");
	private static final String slash = System.getProperty("file.separator");
	static final String dbDirectory = ".goofy";

	private static final String[] previousdbNames = { "infoVaultIII", "goofyIII" };
	private static final String[] dbNames = { "infoVaultIV", "goofyIV" };
	private static final String previousdbName = previousdbNames[infoVault.releaseMarker];
	private static final String dbName = dbNames[infoVault.releaseMarker];
	private static final String dbPath = homeDirectory + slash + dbDirectory;

	private static final String LOCKNAME = ".lock";
	private static final String DBTABLE = "information";
	private static final String DBCOL1 = "name";
	private static final String DBCOL2 = "type";
	private static final String DBCOL3 = "content";
	private static final String DBCOL4 = "style";

	private static final String sqlCREATE = "CREATE TABLE " + DBTABLE + "(" + DBCOL1 + " TEXT NOT NULL PRIMARY KEY, " + DBCOL2 + " TEXT, " + DBCOL3 + " TEXT NULL, " + DBCOL4 + " TEXT NULL);";
	private static final String sqlCOUNT = "SELECT COUNT(*) FROM " + DBTABLE + ";";
	private static final String sqlGETINFO = "SELECT " + DBCOL1 + ", " + DBCOL2 + " FROM " + DBTABLE;
	private static final String sqlGETCONTENT = "SELECT " + DBCOL3 + " FROM " + DBTABLE + " WHERE " + DBCOL1 + "=\"%s\";";
	private static final String sqlGETSTYLE = "SELECT " + DBCOL4 + " FROM " + DBTABLE + " WHERE " + DBCOL1 + "=\"%s\";";
	private static final String sqlDELETE = "DELETE FROM " + DBTABLE + " WHERE " + DBCOL1 + "=\"%s\";";
	private static final String sqlINSERT = "INSERT INTO " + DBTABLE + " (" + DBCOL1 + ", " + DBCOL2 + ", " + DBCOL3 + "," + DBCOL4 + ") VALUES (\"%s\", \"%s\", \"%s\", \"%s\");";
	private static final String sqlUPDATE = "UPDATE " + DBTABLE + " SET " + DBCOL1 + "=\"%s\", " + DBCOL2 + "=\"%s\", " + DBCOL3 + "=\"%s\", " + DBCOL4 + "=\"%s\" WHERE " + DBCOL1 + "=\"%s\";";

	private static Connection connection;
	private static Statement statement;
	private static boolean isDBconnected;
	//--------------------------------------------------------------------------
	private static void connectDB() {
		File f = new File(dbPath + slash + dbName);
		if (f.exists())
			try {
				Class.forName("org.sqlite.JDBC");
				connection = DriverManager.getConnection("jdbc:sqlite:" + dbPath + slash + dbName);
				statement = connection.createStatement();
				isDBconnected = true;
			} catch (ClassNotFoundException e) {
				infoVaultUI.displayError("Connection error: " + e.getMessage(), 1);
				infoVault.Exit(true, -1);
			} catch (SQLException e) {
				infoVaultUI.displayError("Connection error ('create statement' SQL exception): " + e.getMessage(), 1);
				infoVault.Exit(true, -1);
			}
		else {
			File g = new File(dbPath + slash + previousdbName);
			if (g.exists())
				informIncompatibility();
			f = new File(dbPath);
			if (!f.exists() || !f.isDirectory()) //noinspection ResultOfMethodCallIgnored
				f.mkdirs();
			try {
				Class.forName("org.sqlite.JDBC");
				connection = DriverManager.getConnection("jdbc:sqlite:" + dbPath + slash + dbName);
				statement = connection.createStatement();
				isDBconnected = true;
			} catch (ClassNotFoundException e) {
				infoVaultUI.displayError("Connection error: " + e.getMessage(), 1);
				infoVault.Exit(true, -1);
			} catch (SQLException e) {
				infoVaultUI.displayError("Connection error ('create statement' exception): " + e.getMessage(), 1);
				infoVault.Exit(true, -1);
			}
			try {
				statement.executeUpdate(sqlCREATE);
			} catch (SQLException e) {
				infoVaultUI.displayError("Database creation error ('create' SQL exception): " + e.getMessage(), 1);
				infoVault.Exit(true, -1);
			}
		}
	}
	//--------------------------------------------------------------------------
	private static void closeDB() {
		if (!isDBconnected) return;
		try {
			statement.close();
			connection.close();
		} catch (SQLException e) {
			infoVaultUI.displayError("Cannot close database: " + e.getMessage(), 1);
			infoVault.Exit(true, -1);
		}
		isDBconnected = false;
	}
	//--------------------------------------------------------------------------
	static void lockDB(Display d) {
		File f = new File(dbPath + slash + LOCKNAME);
		if (f.exists()) {
			infoVaultUI.shell = new Shell(d, SWT.NONE);
			infoVaultUI.displayError("You are already using this application...", 0);
			infoVault.Exit(false, -1);
		}
		try {
			if (!f.createNewFile()) {
				infoVaultUI.shell = new Shell(d, SWT.NONE);
				infoVaultUI.displayError("Cannot create lock file!", 0);
				infoVault.Exit(false, -1);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	//--------------------------------------------------------------------------
	static void unlockDB() {
		//noinspection ResultOfMethodCallIgnored
		new File(dbPath + slash + LOCKNAME).delete();
	}
	//--------------------------------------------------------------------------
	static void testEncryptionKey() throws Exception {
		if (!isDBconnected) connectDB();
		infoVaultUI.anInformationHeader[] headers = new infoVaultUI.anInformationHeader[getCount()];
		ResultSet rs = statement.executeQuery(sqlGETINFO);
		int i = 0;
		while (rs.next()) {
			headers[i] = new infoVaultUI.anInformationHeader();
			headers[i].setHeader(infoVaultAES.decrypt(infoVaultLogOn.encryptionKey, rs.getString(DBCOL1)));
			headers[i].setType(infoVaultAES.decrypti(infoVaultLogOn.encryptionKey, rs.getString(DBCOL2)));
			++i;
		}
		rs.close();
	}
	//--------------------------------------------------------------------------
	private static int getCount() {
		if (!isDBconnected) connectDB();
		int $ = 0;
		try {
			ResultSet rs = statement.executeQuery(sqlCOUNT);
			rs.next();
			$ = rs.getInt(1);
			rs.close();
		} catch (SQLException e) {
			infoVaultUI.displayError("SQL error ('count' exception): " + e.getMessage(), 1);
			infoVault.Exit(true, -1);
		}
		return $;
	}
	//--------------------------------------------------------------------------
	static infoVaultUI.anInformationHeader[] getHeaders() {
		if (!isDBconnected) connectDB();
		infoVaultUI.anInformationHeader[] headers = new infoVaultUI.anInformationHeader[getCount()];
		try {
			ResultSet rs = statement.executeQuery(sqlGETINFO);
			int i = 0;
			while (rs.next()) {
				headers[i] = new infoVaultUI.anInformationHeader();
				headers[i].setHeader(infoVaultAES.decrypt(infoVaultLogOn.encryptionKey, rs.getString(DBCOL1)));
				headers[i].setType(infoVaultAES.decrypti(infoVaultLogOn.encryptionKey, rs.getString(DBCOL2)));
				++i;
			}
			rs.close();
		} catch (SQLException e) {
			infoVaultUI.displayError("SQL error ('select' exception): " + e.getMessage(), 1);
			infoVault.Exit(true, -1);
		} catch (Exception e) {
			infoVaultUI.displayError("Exception error: " + e.getMessage(), 1);
			infoVault.Exit(true, 1);
		}
		return headers;
	}
	//--------------------------------------------------------------------------
	static void deleteInformation(String header) {
		if (!isDBconnected) connectDB();
		try {
			statement.executeUpdate(String.format(sqlDELETE, infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, header)));
		} catch (SQLException e) {
			infoVaultUI.displayError("SQL error ('delete' exception): " + e.getMessage(), 1);
			infoVault.Exit(true, -1);
		} catch (Exception e) {
			infoVaultUI.displayError("Wrong Encryption Key: " + e.getMessage(), 0);
			infoVault.Exit(false, -1);
		}
		infoVaultDBimg.deleteImages(header);
	}
	//--------------------------------------------------------------------------
	static void insertInformation(int idx) {
		if (!isDBconnected) connectDB();
		int type = infoVaultInformation.getInformationType(idx);
		String style = "";
		String content = "";
		switch (type) {
			case infoVaultUI.TYPE_INFORMATION_EDITOR:
				style = infoVaultInformation.getInformationEditorStyle(idx);
				content = infoVaultInformation.getInformationEditorContent(idx);
				break;
			//case infoVaultUI.TYPE_INFORMATION_TABLE:
			//	style = infoVaultInformation.getInformationTableStyle(idx);
			//	content = infoVaultInformation.getInformationTableContent(idx);
			//	break;
			case infoVaultUI.TYPE_INFORMATION_GRID:
				style = infoVaultInformation.getInformationGridStyle(idx);
				content = infoVaultInformation.getInformationGridContent(idx);
				break;
			default:
				infoVaultUI.displayError("Abnormal situation: Incorrect information type", 2);
				break;
		}
		try {
			statement.executeUpdate(String.format(sqlINSERT,
					infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, infoVaultInformation.getInformationName(idx)),
					infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, type),
					infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, content),
					infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, style)));
		} catch (SQLException e) {
			infoVaultUI.displayError("SQL error ('insert' exception): " + e.getMessage(), 1);
			infoVault.Exit(true, -1);
		} catch (Exception e) {
			infoVaultUI.displayError("Wrong Encryption Key: " + e.getMessage(), 0);
			infoVault.Exit(false, -1);
		}
		infoVaultDBimg.saveImages(infoVaultLogOn.encryptionKey, idx);
	}
	//--------------------------------------------------------------------------
	static void saveInformation(String newKey, int idx) {
		if (!isDBconnected) connectDB();
		int type = infoVaultInformation.getInformationType(idx);
		String style = "";
		String content = "";
		switch (type) {
			case infoVaultUI.TYPE_INFORMATION_EDITOR:
				style = infoVaultInformation.getInformationEditorStyle(idx);
				content = infoVaultInformation.getInformationEditorContent(idx);
				break;
			//case infoVaultUI.TYPE_INFORMATION_TABLE:
			//	style = infoVaultInformation.getInformationTableStyle(idx);
			//	content = infoVaultInformation.getInformationTableContent(idx);
			//	break;
			case infoVaultUI.TYPE_INFORMATION_GRID:
				style = infoVaultInformation.getInformationGridStyle(idx);
				content = infoVaultInformation.getInformationGridContent(idx);
				break;
			default:
				infoVaultUI.displayError("Abnormal situation: Incorrect information type", 2);
				break;
		}
		saveInformation(newKey,
				infoVaultInformation.getInformationName(idx),
				infoVaultInformation.getInformationNameUpdated(idx),
				type, content, style);
		infoVaultDBimg.saveImages(newKey, idx);
	}
	static void saveInformation(String newKey, String header, String newHeader, int type, String content, String style) {
		if (!isDBconnected) connectDB();
		try {
			statement.executeUpdate(String.format(sqlUPDATE,
					infoVaultAES.encrypt(newKey, newHeader),
					infoVaultAES.encrypt(newKey, type),
					infoVaultAES.encrypt(newKey, content),
					infoVaultAES.encrypt(newKey, style),
					infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, header)));
		} catch (SQLException e) {
			infoVaultUI.displayError("SQL error ('update' exception): " + e.getMessage(), 1);
			infoVault.Exit(true, -1);
		} catch (Exception e) {
			infoVaultUI.displayError("Wrong Encryption Key.: " + e.getMessage(), 0);
			infoVault.Exit(false, -1);
		}
	}
	//--------------------------------------------------------------------------
	static String getContent(String header) {
		if (!isDBconnected) connectDB();
		String $ = "";
		try {
			ResultSet rs = statement.executeQuery(String.format(sqlGETCONTENT, infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, header)));
			rs.next();
			$ = infoVaultAES.decrypt(infoVaultLogOn.encryptionKey, rs.getString(DBCOL3));
			rs.close();
		} catch (SQLException e) {
			infoVaultUI.displayError("SQL error ('get content' exception): " + e.getMessage(), 1);
			infoVault.Exit(true, -1);
		} catch (Exception e) {
			infoVaultUI.displayError("Wrong Encryption Key: " + e.getMessage(), 0);
			infoVault.Exit(false, -1);
		}
		return $;
	}
	//--------------------------------------------------------------------------
	static String getStyle(String header) {
		if (!isDBconnected) connectDB();
		String str = "";
		try {
			ResultSet rs = statement.executeQuery(String.format(sqlGETSTYLE, infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, header)));
			rs.next();
			str = infoVaultAES.decrypt(infoVaultLogOn.encryptionKey, rs.getString(DBCOL4));
			rs.close();
		} catch (SQLException e) {
			infoVaultUI.displayError("SQL error ('get style' exception): " + e.getMessage(), 1);
			infoVault.Exit(true, -1);
		} catch (Exception e) {
			infoVaultUI.displayError("Wrong Encryption Key: " + e.getMessage(), 0);
			infoVault.Exit(false, -1);
		}
		return str;
	}
	//--------------------------------------------------------------------------
	//--- EDITOR ---------------------------------------------------------------
	//--------------------------------------------------------------------------
	static Font getEditorFont(String header) {
		if (!isDBconnected) connectDB();
		String str = "";
		try {
			ResultSet rs = statement.executeQuery(String.format(sqlGETSTYLE, infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, header)));
			rs.next();
			str = infoVaultAES.decrypt(infoVaultLogOn.encryptionKey, rs.getString(DBCOL4));
			rs.close();
		} catch (SQLException e) {
			infoVaultUI.displayError("SQL error ('get font' exception): " + e.getMessage(), 1);
			infoVault.Exit(true, -1);
		} catch (Exception e) {
			infoVaultUI.displayError("Wrong Encryption Key: " + e.getMessage(), 0);
			infoVault.Exit(false, -1);
		}
		return deserializationFontString(
				str.substring(str.indexOf(infoVaultInformation.B_FONT) + infoVaultInformation.B_FONT.length(), str.indexOf(infoVaultInformation.E_FONT)));
	}
	//--------------------------------------------------------------------------
	static Color getEditorBackground(String header) {
		if (!isDBconnected) connectDB();
		String str = "";
		try {
			ResultSet rs = statement.executeQuery(String.format(sqlGETSTYLE, infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, header)));
			rs.next();
			str = infoVaultAES.decrypt(infoVaultLogOn.encryptionKey, rs.getString(DBCOL4));
			rs.close();
		} catch (SQLException e) {
			infoVaultUI.displayError("SQL error ('get background' exception): " + e.getMessage(), 1);
			infoVault.Exit(true, -1);
		} catch (Exception e) {
			infoVaultUI.displayError("Wrong Encryption Key: " + e.getMessage(), 0);
			infoVault.Exit(false, -1);
		}
		return deserializationColorString(
				str.substring(str.indexOf(infoVaultInformation.B_BACKGROUND) + infoVaultInformation.B_BACKGROUND.length(), str.indexOf(infoVaultInformation.E_BACKGROUND)));
	}
	//--------------------------------------------------------------------------
	static StyleRange[] getEditorStyleRange(String header) {
		if (!isDBconnected) connectDB();
		String str = "";
		try {
			ResultSet rs = statement.executeQuery(String.format(sqlGETSTYLE, infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, header)));
			rs.next();
			str = infoVaultAES.decrypt(infoVaultLogOn.encryptionKey, rs.getString(DBCOL4));
			rs.close();
		} catch (SQLException e) {
			infoVaultUI.displayError("SQL error ('get style' exception): " + e.getMessage(), 1);
			infoVault.Exit(true, -1);
		} catch (Exception e) {
			infoVaultUI.displayError("Wrong Encryption Key: " + e.getMessage(), 0);
			infoVault.Exit(false, -1);
		}
		return deserializationStyleRangeString(
				str.substring(str.indexOf(infoVaultInformation.B_STYLERANGE) + infoVaultInformation.B_STYLERANGE.length(), str.indexOf(infoVaultInformation.E_STYLERANGE)));
	}
	//--------------------------------------------------------------------------
	// The following pieces of code is to be reviewed. It undoes what
	// "Arrays.toString(StyledText.getStyleRanges(true))" did.
	// To be refactored to make it shorter, more efficient and more readable...
	//--------------------------------------------------------------------------
	private static StyleRange[] deserializationStyleRangeString(String s) {
		s = s.trim();
		if (s.length() <= "[StyleRange ".length()) return null;
		s = s.substring(12);	// Remove "[StyleRange "
		s = s.substring(0, s.length() - 1);	// Remove ending "}"
		String astr[] = s.split("StyleRange");
		StyleRange[] $ = new StyleRange[astr.length];
		for (int i = 0; i < astr.length; ++i) {
			String sval = astr[i].trim();
			if (sval.length() == 0) continue;
			sval = astr[i].trim().substring(1);	// remove {
			if (sval.charAt(sval.length() - 1) == ',')
				sval = sval.substring(0, sval.length() - 2);	// remove '},'
			else
				sval = sval.substring(0, sval.length() - 1);	// remove '}'
			String[] val = sval.split(",");
			if (val.length == 1) continue;
			$[i] = new StyleRange();
			$[i].start = string2int(val[0].trim());
			if (val.length <= 1) continue;
			$[i].length = string2int(val[1].trim());
			if (val.length <= 2) continue;
			$[i].fontStyle = SWT.NORMAL;
			switch (val[2].trim()) {
				case "fontStyle=normal":
					$[i].fontStyle |= SWT.NORMAL;
					break;
				case "fontStyle=bold":
					$[i].fontStyle |= SWT.BOLD;
					break;
				case "fontStyle=italic":
					$[i].fontStyle |= SWT.ITALIC;
					break;
				case "fontStyle=bold-italic":
					$[i].fontStyle |= SWT.BOLD | SWT.ITALIC;
				default:
					break;
			}
			for (int j = 3; j < val.length; ++j) {
				String str = val[j].trim();
				if ("underline=single".equals(str)) {
					$[i].underline = true;
					continue;
				}
				if ("underline=link".equals(str)) {
					$[i].underline = true;
					$[i].underlineStyle = SWT.UNDERLINE_LINK;
					continue;
				}
				if ("striked out".equals(val[j].trim())) {
					$[i].strikeout = true;
				}

			}
			int k;
			if (-1 != (k = sval.indexOf("font=Font {"))) {
				String s1 = sval.substring(k, sval.length()).trim().replace("font=Font {", "");
				s1 = s1.substring(0, s1.indexOf("}"));
				String as[] = s1.split("-");
				if (as.length == 3)
					$[i].font = infoVaultCacheFonts.add(as[0], Integer.valueOf(as[1]), Integer.valueOf(as[2]));
				else
					infoVaultUI.displayWarning("Recorded font inconsistent. Ignored!");
			}
			if (-1 != (k = sval.indexOf("foreground=Color"))) {
				String s1 = sval.substring(k, sval.length()).trim().replace("foreground=Color {", "").replaceAll("}", "");
				val = s1.split(",");
				if (val.length >= 3)
					$[i].foreground = infoVaultCacheColors.get(Integer.valueOf(val[0].trim()),
							Integer.valueOf(val[1].trim()), Integer.valueOf(val[2].trim()));
			}
		}
		return $.length != 0 ? $ : null;
	}
	//=========================================================================
	static Color deserializationColorString(String s) {
		if (s.length() == 0) return null;
		s = s.replace("Color {", "").replaceAll("}", "");
		String[] val = s.split(",");
		return val.length < 3 ? null
				: infoVaultCacheColors.get(Integer.valueOf(val[0].trim()), Integer.valueOf(val[1].trim()),
				Integer.valueOf(val[2].trim()));
	}
	//-------------------------------------------------------------------------
	static Font deserializationFontString(String ¢) {
		if (¢.length() == 0) return null;
		String as[] = ¢.split("-");
		if (as.length == 3)
			return infoVaultCacheFonts.add(as[0], Integer.valueOf(as[1]), Integer.valueOf(as[2]));
		infoVaultUI.displayWarning("Recorded inconsistent font. Ignored!");
		return null;
	}
	//-------------------------------------------------------------------------
	private static int string2int(String s) {
		int n = 0;
		try {
			n = Integer.valueOf(s);
		} catch (Exception e) {
			infoVaultUI.displayError("Abnormal situation: " + e.getMessage(), 2);
		}
		return n;
	}
	//=========================================================================
	static void Exit() {
		closeDB();
	}
	//=========================================================================
	private static void informIncompatibility() {
		MessageBox messageBox = new MessageBox(infoVaultLogOn.shellLogOn, SWT.ICON_WARNING | SWT.YES | SWT.NO );
		messageBox.setText("WARNING");
		messageBox.setMessage("This new version is incompatible with the previous one. A new database will be created. "
				+ "Please backup your information using the previous version to re-inject it into the new database. "
				+ "Are we continuing?");
		if (messageBox.open() == SWT.NO) infoVault.Exit(true, 0);
	}
}
//==============================================================================
