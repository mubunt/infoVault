//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Display;

import java.util.ArrayList;
//------------------------------------------------------------------------------
class infoVaultCacheFonts {
	private static class aFont {
		Font f;
		String f2string;
	}
	private static final ArrayList<aFont> fonts = new ArrayList<>();
	//--------------------------------------------------------------------------
	static Font add(String name, int size, int style) {
		aFont tmpFont = new aFont();
		tmpFont.f = new Font(Display.getCurrent(), name, size, style);
		tmpFont.f2string = "" + tmpFont.f;
		fonts.add(tmpFont);
		return tmpFont.f;
	}

	static Font add(FontData fd) {
		aFont tmpFont = new aFont();
		tmpFont.f = new Font(Display.getCurrent(), fd);
		tmpFont.f2string = "" + tmpFont.f;
		fonts.add(tmpFont);
		return tmpFont.f;
	}
	//--------------------------------------------------------------------------
	static void disposeAll() {
		for (int k = 0; k < fonts.size(); ++k) {
			if (fonts.get(k) == null || fonts.get(k).f == null)
				continue;
			fonts.get(k).f.dispose();
			fonts.set(k, null);
		}
	}
	//--------------------------------------------------------------------------
	static FontData[] get(String s) {
		for (aFont fo : fonts) {
			if (fo.f2string.equals(s)) return fo.f.getFontData();
		}
		return new FontData[] { new FontData("Arial", 10, 1) };
	}
}
//==============================================================================
