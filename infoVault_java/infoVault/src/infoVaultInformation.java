//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.eclipse.nebula.widgets.grid.Grid;
import org.eclipse.nebula.widgets.grid.GridItem;
import org.eclipse.nebula.widgets.pshelf.PShelfItem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import java.util.ArrayList;
import java.util.Arrays;
//------------------------------------------------------------------------------
class infoVaultInformation {
	//--------------------------------------------------------------------------
	static class anImage {
		Image image;
		int offset;
		int type;
		int count;
		boolean tobedeleted;
	}
	private static class anInformation {
		String informationName;
		String informationNameUpdated;
		int informationType;
		PShelfItem informationPShelfItem;
		StyledText informationEditor;
		Grid informationGrid;
		boolean informationModified;
		boolean informationAlreadyExisting;
		int imageCounter;
		final ArrayList<anImage> images = new ArrayList<>();
	}
	//--------------------------------------------------------------------------
	private static final ArrayList<anInformation> setOfInformation = new ArrayList<>();

	static final String B_STYLERANGE = "<STYLERANGE>";
	static final String E_STYLERANGE = "<STYLERANGE/>";
	static final String B_BACKGROUND = "<BACKGROUND>";
	static final String E_BACKGROUND = "<BACKGROUND/>";
	static final String B_FONT = "<FONT>";
	static final String E_FONT = "<FONT/>";
	static final String B_TABLECOLUMN = "<TABLECOLUMN>";
	static final String E_TABLECOLUMN = "<TABLECOLUMN/>";
	static final String B_TABLEITEM = "<TABLEITEM>";
	static final String E_TABLEITEM = "<TABLEITEM/>";
	static final String B_COLUMNSIZE = "<COLUMNSIZE>";
	static final String E_COLUMNSIZE = "<COLUMNSIZE/>";
	static final String B_CELLBACKGROUND = "<CELLBACKGROUND>";
	static final String E_CELLBACKGROUND = "<CELLBACKGROUND/>";
	static final String B_CELLFOREGROUND = "<CELLFOREGROUND>";
	static final String E_CELLFOREGROUND = "<CELLFOREGROUND/>";
	static final String B_CELLFONT = "<CELLFONT>";
	static final String E_CELLFONT = "<CELLFONT/>";
	static final String VALUE_SEPARATOR = "~";
	//--------------------------------------------------------------------------
	static int add(String header, PShelfItem item, int type, boolean exists) {
		anImage tmpImage = new anImage();
		tmpImage.image = null;
		tmpImage.offset = -1;
		tmpImage.count = 0;
		tmpImage.type = SWT.IMAGE_ICO;
		tmpImage.tobedeleted = false;
		anInformation tmp = new anInformation();
		tmp.images.add(tmpImage);

		tmp.informationName = tmp.informationNameUpdated = header;
		tmp.informationType = type;
		tmp.informationPShelfItem = item;
		tmp.informationEditor = null;
		//tmp.informationTable = null;
		tmp.informationGrid = null;
		tmp.informationModified = !infoVaultUI.MODIFIED;
		tmp.informationAlreadyExisting = exists;
		tmp.imageCounter = 0;
		setOfInformation.add(tmp);
		return(setOfInformation.size() - 1);
	}
	//--------------------------------------------------------------------------
	static void remove(int idxInf) {
		if (! setOfInformation.get(idxInf).informationPShelfItem.isDisposed())
			setOfInformation.get(idxInf).informationPShelfItem.dispose();
		setOfInformation.remove(idxInf);
	}
	//--------------------------------------------------------------------------
	static void set(int idxInf, String prevHeader, String newHeader, int type, boolean status, boolean exists) {
		anInformation tmp = setOfInformation.get(idxInf);
		tmp.informationName = prevHeader;
		tmp.informationNameUpdated = newHeader;
		tmp.informationType = type;
		tmp.informationModified = status;
		tmp.informationAlreadyExisting = exists;
		setOfInformation.set(idxInf, tmp);
	}
	//--------------------------------------------------------------------------
	static int indexOf(String information) {
		int $;
		String s = (!information.substring(0, 1).equals(infoVaultUI.ASTERISK) ? information
				: information.substring(1, information.length()));
		for ($ = 0; $ < setOfInformation.size(); ++$)
			if (s.equals(setOfInformation.get($).informationNameUpdated)) break;
		if ($ == setOfInformation.size()) $ = -1;
		return $;
	}
	//--------------------------------------------------------------------------
	static void addImage(int idxInf, int offset, int type, Image image) {
		anImage tmpImage = new anImage();
		tmpImage.image = image;
		tmpImage.offset = offset;
		tmpImage.type = type;
		tmpImage.count = setOfInformation.get(idxInf).imageCounter;
		tmpImage.tobedeleted = false;
		++setOfInformation.get(idxInf).imageCounter;
		if (setOfInformation.get(idxInf).images.get(0).image == null)
			setOfInformation.get(idxInf).images.set(0, tmpImage);
		else
			setOfInformation.get(idxInf).images.add(tmpImage);
	}
	static void addImage(int idxInf, anImage image) {
		setOfInformation.get(idxInf).imageCounter = Math.max(setOfInformation.get(idxInf).imageCounter, image.count) + 1;
		if (setOfInformation.get(idxInf).images.get(0).image == null)
			setOfInformation.get(idxInf).images.set(0, image);
		else
			setOfInformation.get(idxInf).images.add(image);
	}
	//--------------------------------------------------------------------------
	static void postponeDeleteImageOnSave(int idxInf, int idxImg) {
		anImage tmpImage = setOfInformation.get(idxInf).images.get(idxImg);
		tmpImage.tobedeleted = true;
		setOfInformation.get(idxInf).images.set(idxImg, tmpImage);
	}
	//--------------------------------------------------------------------------
	static boolean hasInformationBeenChanged(int idxInf) {
		return setOfInformation.get(idxInf).informationModified;
	}
	static boolean doesInformationExist(int idxInf) {
		return setOfInformation.get(idxInf).informationAlreadyExisting;
	}
	//--------------------------------------------------------------------------
	static String getInformationName(int idxInf) {
		return setOfInformation.get(idxInf).informationName;
	}
	static String getInformationNameUpdated(int idxInf) {
		return setOfInformation.get(idxInf).informationNameUpdated;
	}
	static int getInformationType(int idxInf) { return setOfInformation.get(idxInf).informationType; }
	static PShelfItem getInformationPShelfItem(int idxInf) { return setOfInformation.get(idxInf).informationPShelfItem; }
	//--------------------------------------------------------------------------
	static StyledText getInformationEditor(int idxInf) {
		return setOfInformation.get(idxInf).informationEditor;
	}
	static String getInformationEditorContent(int idxInf) {
		return setOfInformation.get(idxInf).informationEditor.getText();
	}
	static String getInformationEditorStyle(int idxInf) {
		return B_STYLERANGE
				+ styleToHumanReadableString(Arrays.toString(getInformationEditor(idxInf).getStyleRanges(true)))
				+ E_STYLERANGE
				+ B_BACKGROUND
				+ "" + getInformationEditor(idxInf).getBackground()
				+ E_BACKGROUND
				+ B_FONT
				+ fontToHumanReadableString(getInformationEditor(idxInf).getFont().getFontData())
				+ E_FONT;
	}
	//--------------------------------------------------------------------------
	static Grid getInformationGrid(int idxInf) {
		return setOfInformation.get(idxInf).informationGrid;
	}
	static String getInformationGridContent(int idxInf) {
		StringBuilder $ = new StringBuilder(B_TABLECOLUMN);
		for (int col = 0; col < getInformationGrid(idxInf).getColumnCount(); ++col) {
			String ss = getInformationGrid(idxInf).getColumn(col).toString().replace("GridColumn {", "");
			ss = ss.substring(0, ss.length() - 1);
			if (! $.substring($.length() - 1).equals(">")) $.append(VALUE_SEPARATOR);
			$.append("\"").append(ss).append("\"");
		}
		$.append(E_TABLECOLUMN);
		for (int row = 0; row < getInformationGrid(idxInf).getItemCount(); ++row) {
			$.append(B_TABLEITEM);
			GridItem itm = getInformationGrid(idxInf).getItem(row);
			for (int col = 0; col < getInformationGrid(idxInf).getColumnCount(); ++col) {
				if (! $.substring($.length() - 1).equals(">")) $.append(VALUE_SEPARATOR);
				$.append("\"").append(itm.getText(col)).append("\"");
			}
			$.append(E_TABLEITEM);
		}
		return $.toString();
	}
	static String getInformationGridContentCSV(int idxInf) {
		StringBuilder $ = new StringBuilder("");
		for (int row = 0; row < getInformationGrid(idxInf).getItemCount(); ++row) {
			GridItem itm = getInformationGrid(idxInf).getItem(row);
			for (int col = 0; col < getInformationGrid(idxInf).getColumnCount(); ++col) {
				if (col != 0) $.append(",");
				$.append(itm.getText(col));
			}
			$.append("\n");
		}
		return $.toString();
	}
	static String getInformationGridStyle(int idxInf) {
		StringBuilder $ = new StringBuilder(B_COLUMNSIZE);
		for (int col = 0; col < getInformationGrid(idxInf).getColumnCount(); ++col) {
			if (! $.substring($.length() - 1).equals(">")) $.append(VALUE_SEPARATOR);
			$.append("").append(getInformationGrid(idxInf).getColumn(col).getWidth());
		}
		$.append(E_COLUMNSIZE + B_CELLBACKGROUND);
		StringBuilder sFore = new StringBuilder(B_CELLFOREGROUND);
		StringBuilder sFont = new StringBuilder(B_CELLFONT);
		Color defaultBackground = new Color(Display.getCurrent(), 255, 255, 255);
		Color defaultForeground = new Color(Display.getCurrent(), 92, 97, 108);
		for (int row = 0; row < getInformationGrid(idxInf).getItemCount(); ++row) {
			GridItem itm = getInformationGrid(idxInf).getItem(row);
			//for (int col = 1; col < getInformationGrid(idxInf).getColumnCount(); ++col) {
			for (int col = 0; col < getInformationGrid(idxInf).getColumnCount(); ++col) {
				Color background = itm.getBackground(col);
				if (background != null && ! background.equals(defaultBackground)) {
					if (!$.substring($.length() - 1).equals(">")) $.append(VALUE_SEPARATOR);
					$.append(row).append("/").append(col).append("/").append(background);
				}

				Color foreground = itm.getForeground(col);
				if (foreground != null && ! foreground.equals(defaultForeground)) {
					if (!sFore.substring(sFore.length() - 1).equals(">")) sFore.append(VALUE_SEPARATOR);
					sFore.append(row).append("/").append(col).append("/").append(foreground);
				}

				Font font = itm.getFont(col);
				if (font != null && ! font.equals(infoVaultLogOn.fontCell)) {
					if (!sFont.substring(sFont.length() - 1).equals(">")) sFont.append(VALUE_SEPARATOR);
					sFont.append(row).append("/").append(col).append("/").append(fontToHumanReadableString(font.getFontData()));
				}
			}
		}
		$.append(E_CELLBACKGROUND).append(sFore).append(E_CELLFOREGROUND).append(sFont).append(E_CELLFONT);
		return $.toString();
	}
	//--------------------------------------------------------------------------
	static anImage getInformationImage(int idxInf, int idxImg) {
		return setOfInformation.get(idxInf).images.get(idxImg);
	}
	static int getImageNumberOf(int idxInf) {
		return setOfInformation.get(idxInf).images.size();
	}
	//--------------------------------------------------------------------------
	static void setInformationEditor(int idxInf, StyledText editor) {
		setOfInformation.get(idxInf).informationEditor = editor;
	}
	//static void setInformationTable(int idxInf, Table table) {
	//	setOfInformation.get(idxInf).informationTable = table;
	//}
	static void setInformationGrid(int idxInf, Grid grid) {
		setOfInformation.get(idxInf).informationGrid = grid;
	}
	static void setImageOffset(int idxInf, int idxImg, int offset) {
		setOfInformation.get(idxInf).images.get(idxImg).offset = offset;
	}
	//--------------------------------------------------------------------------
	static int size() {
		return setOfInformation.size();
	}
	//=========================================================================
	private static String fontToHumanReadableString(FontData[] ¢) {
		return ¢[0].getName() + "-" + ¢[0].getHeight() + "-" + ¢[0].getStyle();
	}
	//-------------------------------------------------------------------------
	private static String styleToHumanReadableString(String s) {
		String prefix = "font=";
		int k;
		String s0 = s;
		while (-1 != (k = s0.indexOf(prefix))) {
			String s1 = s0.substring(k + prefix.length());
			s1 = s1.substring(0, s1.indexOf("}") + 1);
			FontData fd[] = infoVaultCacheFonts.get(s1);
			s = s.replace(s1, "Font {" + fontToHumanReadableString(fd) + "}");
			s0 = s0.substring(k + prefix.length() + s1.length());
		}
		return s;
	}
}
//==============================================================================
