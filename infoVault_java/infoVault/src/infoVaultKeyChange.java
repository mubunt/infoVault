//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.sql.SQLException;
//------------------------------------------------------------------------------
class infoVaultKeyChange {
	private static String newEncryptionKey;
	//--------------------------------------------------------------------------
	static void view(Display d) {
		infoVaultLogOn.encryptionKey = newEncryptionKey = "";

		infoVaultUI.shell = new Shell(d, SWT.CLOSE | SWT.TITLE);
		infoVaultUI.shell.setLayout(new GridLayout());
		infoVaultUI.shell.setBackground(d.getSystemColor(SWT.COLOR_BLACK));
		infoVaultUI.shell.setForeground(d.getSystemColor(SWT.COLOR_WHITE));
		infoVaultUI.shell.setBackgroundMode(SWT.INHERIT_DEFAULT);
		infoVaultUI.shell.setText(infoVault.sEncryptionKeyTitle);

		Composite container = new Composite(infoVaultUI.shell, SWT.NONE);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout layout = new GridLayout(1, false);
		container.setLayout(layout);
		//----------------------------------------------------------------------
		Composite key1 = new Composite(container, SWT.FILL);
		key1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 0, 0));
		key1.setLayout(new GridLayout(3, false));

		final GridData data = new GridData();
		data.grabExcessHorizontalSpace = true;
		data.horizontalAlignment = GridData.FILL;
		data.grabExcessVerticalSpace = false;
		data.verticalAlignment = GridData.FILL;
		data.widthHint = 130;
		//----------------------------------------------------------------------
		Label lab1 = new Label(key1, SWT.NONE);
		lab1.setText("Encryption Key: 0x");
		lab1.setFont(infoVaultLogOn.fontLabel);
		lab1.setForeground(d.getSystemColor(SWT.COLOR_WHITE));

		Text txtKey1 = new Text(key1, SWT.BORDER | SWT.PASSWORD);
		txtKey1.setLayoutData(data);
		txtKey1.setTextLimit(16);
		txtKey1.setFont(infoVaultLogOn.fontText);
		txtKey1.setBackground(d.getSystemColor(SWT.COLOR_WHITE));
		txtKey1.setForeground(d.getSystemColor(SWT.COLOR_BLACK));
		txtKey1.forceFocus();
		txtKey1.addTraverseListener(e -> {
			if (e.detail == SWT.TRAVERSE_RETURN) {
				e.doit = false;
				e.detail = SWT.TRAVERSE_NONE;
			}
		});

		Button ok1 = new Button(key1, SWT.PUSH);
		ok1.setText("OK");
		ok1.setFont(infoVaultLogOn.fontButton);
		ok1.setBackground(d.getSystemColor(SWT.COLOR_BLACK));
		ok1.setForeground(d.getSystemColor(SWT.COLOR_WHITE));
		ok1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 0, 0));
		//----------------------------------------------------------------------
		Label lab2 = new Label(key1, SWT.NONE);
		lab2.setText("New Encryption Key: 0x");
		lab2.setFont(infoVaultLogOn.fontLabel);
		lab2.setForeground(d.getSystemColor(SWT.COLOR_WHITE));

		Text txtKey2 = new Text(key1, SWT.BORDER | SWT.PASSWORD);
		txtKey2.setLayoutData(data);
		txtKey2.setTextLimit(16);
		txtKey2.setFont(infoVaultLogOn.fontText);
		txtKey2.setBackground(d.getSystemColor(SWT.COLOR_WHITE));
		txtKey2.setForeground(d.getSystemColor(SWT.COLOR_BLACK));
		txtKey2.addTraverseListener(e -> {
			if (e.detail == SWT.TRAVERSE_RETURN) {
				e.doit = false;
				e.detail = SWT.TRAVERSE_NONE;
			}
		});

		Button ok2 = new Button(key1, SWT.PUSH);
		ok2.setText("OK");
		ok2.setFont(infoVaultLogOn.fontButton);
		ok2.setBackground(d.getSystemColor(SWT.COLOR_BLACK));
		ok2.setForeground(d.getSystemColor(SWT.COLOR_WHITE));
		ok2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 0, 0));
		//----------------------------------------------------------------------
		ProgressBar bar = new ProgressBar(container, SWT.SMOOTH | SWT.HORIZONTAL);
		GridData gd = new GridData();
		gd.grabExcessVerticalSpace = gd.grabExcessHorizontalSpace = false;
		gd.heightHint = gd.minimumHeight = 20;
		gd.horizontalAlignment = GridData.FILL;
		gd.horizontalIndent = 0;
		gd.verticalAlignment = GridData.CENTER;
		gd.verticalIndent = 0;
		bar.setLayoutData(gd);
		bar.setVisible(false);
		//----------------------------------------------------------------------
		final GridLayout layout2 = new GridLayout();
		layout2.numColumns = 3;
		layout2.makeColumnsEqualWidth = true;

		final GridData data2 = new GridData(SWT.FILL, SWT.BOTTOM, true, false);
		data2.grabExcessHorizontalSpace = true;
		data2.grabExcessVerticalSpace = false;

		Composite but = new Composite(container, SWT.NONE);
		but.setLayout(layout2);
		but.setLayoutData(data2);
		//----------------------------------------------------------------------
		Label invisibleLabel1 = new Label(but, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		invisibleLabel1.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		Label invisibleLabel2 = new Label(but, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		invisibleLabel2.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		//----------------------------------------------------------------------
		Button changeKey = new Button(but, SWT.PUSH);
		changeKey.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, true, 1, 1));
		changeKey.setText("Change Encryption Key");
		changeKey.setFont(infoVaultLogOn.fontButton);
		changeKey.setBackground(d.getSystemColor(SWT.COLOR_BLACK));
		changeKey.setForeground(d.getSystemColor(SWT.COLOR_WHITE));
		changeKey.setEnabled(false);
		//----------------------------------------------------------------------
		ok1.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				changeKey.setEnabled(false);
				infoVaultLogOn.encryptionKey = infoVaultAES.encryptionKey(txtKey1.getText());
				if (infoVaultLogOn.encryptionKey.length() != 0) {
					try {
						infoVaultDB.testEncryptionKey();
					} catch (SQLException e) {
						infoVaultUI.displayError("SQL error ('test encryption key' exception): " + e.getMessage(), 0);
						infoVault.Exit(true, -1);
					} catch (Exception e) {
						infoVaultUI.displayError("Wrong Encryption Key...", 0);
						infoVaultLogOn.encryptionKey = "";
						infoVaultUI.shell.forceActive();
						return;
					}
					if (newEncryptionKey.length() == 0) return;
					changeKey.setEnabled(true);
					return;
				}
				infoVaultUI.displayError("Bad encryption key. Retry please.", 0);
				infoVaultUI.shell.forceActive();
			}
		});

		ok2.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				newEncryptionKey = infoVaultAES.encryptionKey(txtKey2.getText());
				if (newEncryptionKey.length() != 0) {
					if (infoVaultLogOn.encryptionKey.length() == 0) return;
					changeKey.setEnabled(true);
					return;
				}
				infoVaultUI.displayError("Bad new encryption key. Retry please.", 0);
				changeKey.setEnabled(false);
				infoVaultUI.shell.forceActive();
			}
		});

		changeKey.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				infoVaultUI.anInformationHeader[] s = infoVaultDB.getHeaders();
				bar.setMaximum(s.length - 1);
				bar.setVisible(true);
				changeKey.setEnabled(false);
				changeEncryptionKey(s, bar);
				infoVaultUI.displayWarning("Encryption key updated. Ready to continue.");
				infoVaultUI.shell.dispose();
			}
		});
		//----------------------------------------------------------------------
		infoVaultUI.shell.pack();
		Rectangle shellRect = infoVaultUI.shell.getBounds();
		Rectangle displayRect = d.getBounds();
		infoVaultUI.shell.setLocation((displayRect.width - shellRect.width) / 2, (displayRect.height - shellRect.height) / 2);
		infoVaultUI.shell.open();
		while (!infoVaultUI.shell.isDisposed())
			if (!d.readAndDispatch())
				d.sleep();
	}
	//--------------------------------------------------------------------------
	private static void changeEncryptionKey(infoVaultUI.anInformationHeader[] headers, ProgressBar b) {
		for (int ¢ = 0; ¢ < headers.length; ++¢) {
			b.setSelection(¢);
			int idx = infoVaultInformation.add(headers[¢].getHeader(), null, headers[¢].getType(), infoVaultUI.EXISTING);
			infoVaultDBimg.getImages(idx);
			infoVaultDB.saveInformation(newEncryptionKey,
					headers[¢].getHeader(), headers[¢].getHeader(),
					headers[¢].getType(),
					infoVaultDB.getContent(headers[¢].getHeader()),
					infoVaultDB.getStyle(headers[¢].getHeader()));
			infoVaultDBimg.saveImages(newEncryptionKey, idx);
		}
	}
}
//==============================================================================
