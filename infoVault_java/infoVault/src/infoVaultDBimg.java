//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.widgets.Display;

import java.io.*;
import java.sql.*;
//------------------------------------------------------------------------------
class infoVaultDBimg {
	private static final String homeDirectory = System.getProperty("user.home");
	private static final String slash = System.getProperty("file.separator");

	private static final String[] dbNames = { "infoVaultimgIV", "plutoIV" };
	private static final String dbName = dbNames[infoVault.releaseMarker];

	private static final String dbPath = homeDirectory + slash + infoVaultDB.dbDirectory;
	private static final String DBTABLE = "images";
	private static final String DBCOL1 = "name";
	private static final String DBCOL2 = "id";
	private static final String DBCOL3 = "image";
	private static final String DBCOL4 = "type";
	private static final String DBCOL5 = "offset";

	private static final String sqlCREATE =
			"CREATE TABLE " + DBTABLE + "("
					+ DBCOL1 + " TEXT NOT NULL, "
					+ DBCOL2 + " TEXT NOT NULL, "
					+ DBCOL3 + " BLOB NULL, "
					+ DBCOL4 + " TEXT NOT NULL, "
					+ DBCOL5 + " TEXT NOT NULL);";
	private static final String sqlGETIMAGES =
			"SELECT " + DBCOL2 + ", " + DBCOL3 + ", " + DBCOL4 + ", " + DBCOL5 + " FROM " + DBTABLE
					+ " WHERE " + DBCOL1 + "=\"%s\";";
	private static final String sqlDELETEINFORMATION =
			"DELETE FROM " + DBTABLE + " WHERE " + DBCOL1 + "=\"%s\";";
	private static final String sqlDELETEIMAGE =
			"DELETE FROM " + DBTABLE + " WHERE " + DBCOL1 + "=\"%s\" AND " + DBCOL2 + "=\"%s\";";
	private static final String sqlINSERTIMAGE =
			"INSERT INTO " + DBTABLE + " (" + DBCOL1 + ", " + DBCOL2 + ", " + DBCOL3 + ", " + DBCOL4 + ", " + DBCOL5 + ") VALUES (?, ?, ?, ?, ?);";
	private static final String sqlUPDATEIMAGE =
			"UPDATE " + DBTABLE + " SET " + DBCOL1 + "=?, " + DBCOL2 + "=?, "+ DBCOL3 + "=?, " + DBCOL4 + "=?, " + DBCOL5 + "=?"
					+ " WHERE " + DBCOL1 + "=? AND " + DBCOL2 + "=?;";
	private static final String sqlEXISTIMAGE =
			"SELECT 1 FROM " + DBTABLE + " WHERE " + DBCOL1 + "= ? AND " + DBCOL2 + "=?;";
	//private static final String sqlGETINDEXES =
	//		"SELECT " + DBCOL2 + " FROM " + DBTABLE + " WHERE " + DBCOL1 + "=\"%s\";";
	//private static final String sqlGETIMAGE =
	//		"SELECT " + DBCOL3 + " FROM " + DBTABLE + " WHERE " + DBCOL1 + "=\"%s\" AND " + DBCOL2 + "=\"%s\";";

	private static Connection connection;
	private static Statement statement;
	private static boolean isDBconnected;
	//--------------------------------------------------------------------------
	private static void connectDB() {
		File f = new File(dbPath + slash + dbName);
		if (f.exists())
			try {
				Class.forName("org.sqlite.JDBC");
				connection = DriverManager.getConnection("jdbc:sqlite:" + dbPath + slash + dbName);
				statement = connection.createStatement();
				isDBconnected = true;
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				infoVaultUI.displayError("Connection error: " + e.getMessage(), 1);
				infoVault.Exit(infoVault.TOBEUNLOCKED, -1);
			} catch (SQLException e) {
				infoVaultUI.displayError("Connection error ('create statement' SQL exception): " + e.getMessage(), 1);
				infoVault.Exit(infoVault.TOBEUNLOCKED, -1);
			}
		else {
			f = new File(dbPath);
			if (!f.exists() || !f.isDirectory())
				//noinspection ResultOfMethodCallIgnored
				f.mkdirs();
			try {
				Class.forName("org.sqlite.JDBC");
				connection = DriverManager.getConnection("jdbc:sqlite:" + dbPath + slash + dbName);
				statement = connection.createStatement();
				isDBconnected = true;
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				infoVaultUI.displayError("Connection error: " + e.getMessage(), 1);
				infoVault.Exit(infoVault.TOBEUNLOCKED, -1);
			} catch (SQLException e) {
				e.printStackTrace();
				infoVaultUI.displayError("Connection error ('create statement' exception): " + e.getMessage(), 1);
				infoVault.Exit(infoVault.TOBEUNLOCKED, -1);
			}
			try {
				statement.executeUpdate(sqlCREATE);
			} catch (SQLException e) {
				infoVaultUI.displayError("Database creation error ('image create' SQL exception): " + e.getMessage(), 1);
				infoVault.Exit(infoVault.TOBEUNLOCKED, -1);
			}
		}
	}
	//--------------------------------------------------------------------------
	private static void closeDB() {
		if (!isDBconnected) return;
		try {
			statement.close();
			connection.close();
		} catch (SQLException e) {
			infoVaultUI.displayError("Cannot close image database.: " + e.getMessage(), 1);
			infoVault.Exit(infoVault.TOBEUNLOCKED, -1);
		}
		isDBconnected = false;
	}
	//--------------------------------------------------------------------------
	static void getImages(int idx) {
		if (!isDBconnected) connectDB();
		try {
			ResultSet rs = statement.executeQuery(String.format(sqlGETIMAGES,
					infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, infoVaultInformation.getInformationName(idx))));
			while (rs.next()) {
				infoVaultInformation.anImage tmpImage = new infoVaultInformation.anImage();
				tmpImage.image = ba2image(infoVaultAES.decrypt(infoVaultLogOn.encryptionKey, rs.getBytes(DBCOL3)));
				tmpImage.count = infoVaultAES.decrypti(infoVaultLogOn.encryptionKey, rs.getString(DBCOL2));
				tmpImage.type = infoVaultAES.decrypti(infoVaultLogOn.encryptionKey, rs.getString(DBCOL4));
				tmpImage.offset = infoVaultAES.decrypti(infoVaultLogOn.encryptionKey, rs.getString(DBCOL5));
				if (tmpImage.image == null) continue;
				infoVaultInformation.addImage(idx, tmpImage);
			}
			rs.close();
		} catch (SQLException e) {
			infoVaultUI.displayError("SQL error ('image add' exception): " + e.getMessage(), 1);
			infoVault.Exit(infoVault.TOBEUNLOCKED, -1);
		} catch (Exception e) {
			infoVaultUI.displayError("Wrong Encryption Key: " + e.getMessage(), 0);
		}
	}
	//--------------------------------------------------------------------------
	static void deleteImages(String name) {
		if (!isDBconnected) connectDB();
		try {
			statement.executeUpdate(String.format(sqlDELETEINFORMATION, infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, name)));
		} catch (SQLException e) {
			infoVaultUI.displayError("SQL error ('images delete' exception): " + e.getMessage(), 1);
			infoVault.Exit(infoVault.TOBEUNLOCKED, -1);
		} catch (Exception e) {
			infoVaultUI.displayError("Wrong Encryption Key: " + e.getMessage(), 0);
		}
	}
	//--------------------------------------------------------------------------
	static void saveImages(String newKey, int idx) {
		if (!isDBconnected) connectDB();
		infoVaultInformation.anImage tmpImage;
		for (int i = 0; i < infoVaultInformation.getImageNumberOf(idx); ++i) {
			tmpImage = infoVaultInformation.getInformationImage(idx, i);
			if (tmpImage == null || tmpImage.image == null) continue;
			if (tmpImage.tobedeleted) {
				try {
					statement.executeUpdate(String.format(sqlDELETEIMAGE,
							infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, infoVaultInformation.getInformationName(idx)),
							infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, tmpImage.count)));
				} catch (SQLException e) {
					infoVaultUI.displayError("SQL error ('image delete' exception): " + e.getMessage(), 1);
					infoVault.Exit(infoVault.TOBEUNLOCKED, -1);
				} catch (Exception e) {
					infoVaultUI.displayError("Wrong Encryption Key: " + e.getMessage(), 0);
				}
			} else {
				try {
					PreparedStatement preparedStatement = connection.prepareStatement(sqlEXISTIMAGE);
					preparedStatement.setString(1, infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, infoVaultInformation.getInformationName(idx)));
					preparedStatement.setString(2, infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, tmpImage.count));
					ResultSet rs = preparedStatement.executeQuery();
					if (rs.next()) {
						preparedStatement = connection.prepareStatement(sqlUPDATEIMAGE);
						preparedStatement.setString(1, infoVaultAES.encrypt(newKey, infoVaultInformation.getInformationNameUpdated(idx)));
						preparedStatement.setString(2, infoVaultAES.encrypt(newKey, tmpImage.count));
						preparedStatement.setBytes(3, infoVaultAES.encrypt(newKey, image2ba(tmpImage.image, tmpImage.type)));
						preparedStatement.setString(4, infoVaultAES.encrypt(newKey, tmpImage.type));
						preparedStatement.setString(5, infoVaultAES.encrypt(newKey, tmpImage.offset));
						preparedStatement.setString(6, infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, infoVaultInformation.getInformationName(idx)));
						preparedStatement.setString(7, infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, tmpImage.count));
					} else {
						preparedStatement = connection.prepareStatement(sqlINSERTIMAGE);
						preparedStatement.setString(1, infoVaultAES.encrypt(newKey, infoVaultInformation.getInformationName(idx)));
						preparedStatement.setString(2, infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, tmpImage.count));
						preparedStatement.setBytes(3, infoVaultAES.encrypt(newKey, image2ba(tmpImage.image, tmpImage.type)));
						preparedStatement.setString(4, infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, tmpImage.type));
						preparedStatement.setString(5, infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, tmpImage.offset));
					}
					preparedStatement.executeUpdate();
				} catch (SQLException e) {
					infoVaultUI.displayError("SQL error ('images save' exception): " + e.getMessage(), 1);
					infoVault.Exit(infoVault.TOBEUNLOCKED, -1);
				} catch (Exception e) {
					infoVaultUI.displayError("Wrong Encryption Key: " + e.getMessage(), 0);
				}
			}
		}
	}
	//-------------------------------------------------------------------------
	/*
	static Image getImage(String name, String idx) {
		if (!isDBconnected) connectDB();
		Image image = null;
		try {
			ResultSet rs = statement.executeQuery(String.format(sqlGETIMAGE,
					infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, name),
					infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, idx)));
			rs.next();
			image = ba2image(infoVaultAES.decrypt(infoVaultLogOn.encryptionKey, rs.getBytes(DBCOL3)));
			rs.close();
		} catch (SQLException e) {
			infoVaultUI.displayError("SQL error ('image add' exception): " + e.getMessage(), 1);
			infoVault.Exit(infoVault.TOBEUNLOCKED, -1);
		} catch (Exception e) {
			infoVaultUI.displayError("Wrong Encryption Key: " + e.getMessage(), 0);
		}
		return image;
	}
	*/
	//--------------------------------------------------------------------------
	/*
	static String[] getIndex(String name) {
		if (!isDBconnected) connectDB();
		ArrayList<String> lstIndexes = new ArrayList<>();
		try {
			ResultSet rs = statement.executeQuery(String.format(sqlGETINDEXES, infoVaultAES.encrypt(infoVaultLogOn.encryptionKey, name)));
			while (rs.next()) {
				lstIndexes.add(infoVaultAES.decrypt(infoVaultLogOn.encryptionKey, rs.getString(DBCOL2)));
			}
			rs.close();
		} catch (SQLException e) {
			infoVaultUI.displayError("SQL error ('get indexes' exception): " + e.getMessage(), 1);
			infoVault.Exit(infoVault.TOBEUNLOCKED, -1);
		} catch (Exception e) {
			infoVaultUI.displayError("Wrong Encryption Key: " + e.getMessage(), 0);
		}
		return lstIndexes.stream().toArray(String[]::new);
	}
	*/
	//-------------------------------------------------------------------------
	static void Exit() {
		closeDB();
	}
	//-------------------------------------------------------------------------
	private static byte[] image2ba(Image image, int type) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream writeOut = new DataOutputStream(out);
		ImageLoader loader = new ImageLoader();
		ImageData imageData2 = image.getImageData();
		loader.data = new ImageData[] { imageData2 };
		loader.save(writeOut, type);
		try {
			writeOut.close();
			return out.toByteArray();
		} catch (IOException e) {
			infoVaultUI.displayError("Wrong Image Encoding: " + e.getMessage(), 0);
			return null;
		}
	}

	private static Image ba2image(byte[] ba) {
		BufferedInputStream inputStreamReader = new BufferedInputStream(new ByteArrayInputStream(ba));
		try {
			ImageData imageData = new ImageData(inputStreamReader);
			return new Image(Display.getDefault(), imageData);
		} catch(Exception e) {
			infoVaultUI.displayError("Wrong Image Decoding: " + e.getMessage(), 0);
			return null;
		}
	}
}
//==============================================================================
