//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.PaintObjectEvent;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.*;
//------------------------------------------------------------------------------
class infoVaultEditor {
	private static final String MARKER = "\uFFFC";
	//--------------------------------------------------------------------------
	static StyledText editorArea(Composite parent, int idx) {
		String name = infoVaultInformation.getInformationName(idx);

		final GridData gd = new GridData();
		gd.grabExcessHorizontalSpace = true;
		gd.horizontalAlignment = GridData.FILL;
		gd.grabExcessVerticalSpace = true;
		gd.verticalAlignment = GridData.FILL;

		StyledText $ = new StyledText(parent, SWT.WRAP | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		$.setFont(infoVaultLogOn.fontText);
		$.setLayoutData(gd);
		if (infoVaultInformation.doesInformationExist(idx)) {
			// --Text content
			$.setText(infoVaultDB.getContent(name));
			// --Background color
			Color bg = infoVaultDB.getEditorBackground(name);
			if (bg != null) $.setBackground(bg);
			// --Fonts
			Font font = infoVaultDB.getEditorFont(name);
			if (font != null) $.setFont(font);
			// --Style
			StyleRange[] stylerange = infoVaultDB.getEditorStyleRange(name);
			if (stylerange != null)
				try {
					$.setStyleRanges(stylerange);
				} catch (Exception e) {
					infoVaultUI.displayError("Abnormal situation (StyleRange): " + e.getMessage(), 2);
				}
			// --Images
			infoVaultDBimg.getImages(idx);
			for (int ¢ = 0; ¢ < infoVaultInformation.getImageNumberOf(idx); ++¢) {
				if (infoVaultInformation.getInformationImage(idx, ¢).image == null) continue;
				infoVaultUI.insertImage($,
						infoVaultInformation.getInformationImage(idx, ¢).offset,
						infoVaultInformation.getInformationImage(idx, ¢).image);
			}
		}
		// Verify listener to keep the image offsets up to date
		$.addVerifyListener((VerifyEvent e) -> {
			int start = e.start;
			int replaceCharCount = e.end - e.start;
			int newCharCount = e.text.length();
			int index = infoVaultUI.getCurrentPShelf();
			for (int i = 0; i < infoVaultInformation.getImageNumberOf(index); i++) {
				int offset = infoVaultInformation.getInformationImage(index, i).offset;
				if (start <= offset && offset < start + replaceCharCount) {
					// this image is being deleted from the text
					infoVaultInformation.postponeDeleteImageOnSave(index, i);
					offset = -1;
				}
				if (offset != -1 && offset >= start) {
					offset += newCharCount - replaceCharCount;
					infoVaultInformation.setImageOffset(index, i, offset);
				}
			}
		});
		$.addPaintObjectListener((PaintObjectEvent e) -> {
			int index = infoVaultUI.getCurrentPShelf();
			for (int ¢ = 0; ¢ < infoVaultInformation.getImageNumberOf(index); ++¢) {
				int offset = infoVaultInformation.getInformationImage(index, ¢).offset;
				if (e.style.start == offset) {
					Image image = infoVaultInformation.getInformationImage(index, ¢).image;
					e.gc.drawImage(image, e.x, (e.y + e.ascent - e.style.metrics.ascent));
				}
			}
		});
		$.addModifyListener(__ -> infoVaultUI.markInformationAsModified(infoVaultUI.getCurrentPShelf()));
		$.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.stateMask == SWT.CTRL && e.keyCode == 'a') {
					$.selectAll();
					e.doit = false;
				}
			}
		});
		return $;
	}
	//--------------------------------------------------------------------------
	static final SelectionAdapter listenerStyleBold = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			int current = infoVaultUI.getCurrentPShelf();
			Point sel = infoVaultInformation.getInformationEditor(current).getSelectionRange();
			if (sel == null || sel.y == 0) return;
			StyleRange style;
			for (int i = sel.x; i < sel.x + sel.y; ++i) {
				StyleRange range = infoVaultInformation.getInformationEditor(current).getStyleRangeAtOffset(i);
				if (range == null)
					style = new StyleRange(i, 1, null, null, SWT.NORMAL);
				else {
					style = (StyleRange) range.clone();
					style.start = i;
					style.length = 1;
				}
				style.fontStyle ^= SWT.BOLD;
				infoVaultInformation.getInformationEditor(current).setStyleRange(style);
			}
			infoVaultUI.markInformationAsModified(current);
		}
	};

	static final SelectionAdapter listenerStyleItalic = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			int current = infoVaultUI.getCurrentPShelf();
			Point sel = infoVaultInformation.getInformationEditor(current).getSelectionRange();
			if (sel == null || sel.y == 0) return;
			StyleRange style;
			for (int i = sel.x; i < sel.x + sel.y; ++i) {
				StyleRange range = infoVaultInformation.getInformationEditor(current).getStyleRangeAtOffset(i);
				if (range == null)
					style = new StyleRange(i, 1, null, null, SWT.NORMAL);
				else {
					style = (StyleRange) range.clone();
					style.start = i;
					style.length = 1;
				}
				style.fontStyle ^= SWT.ITALIC;
				infoVaultInformation.getInformationEditor(current).setStyleRange(style);
			}
			infoVaultUI.markInformationAsModified(current);
		}
	};

	static final SelectionAdapter listenerStyleUnderline = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			int current = infoVaultUI.getCurrentPShelf();
			Point sel = infoVaultInformation.getInformationEditor(current).getSelectionRange();
			if (sel == null || sel.y == 0) return;
			StyleRange style;
			for (int i = sel.x; i < sel.x + sel.y; ++i) {
				StyleRange range = infoVaultInformation.getInformationEditor(current).getStyleRangeAtOffset(i);
				if (range == null)
					style = new StyleRange(i, 1, null, null, SWT.NORMAL);
				else {
					style = (StyleRange) range.clone();
					style.start = i;
					style.length = 1;
				}
				style.underline = !style.underline;
				infoVaultInformation.getInformationEditor(current).setStyleRange(style);
			}
			infoVaultUI.markInformationAsModified(current);
		}
	};

	static final SelectionAdapter listenerStyleStrikeOut = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			int current = infoVaultUI.getCurrentPShelf();
			Point sel = infoVaultInformation.getInformationEditor(current).getSelectionRange();
			if (sel == null || sel.y == 0) return;
			StyleRange style;
			for (int i = sel.x; i < sel.x + sel.y; ++i) {
				StyleRange range = infoVaultInformation.getInformationEditor(current).getStyleRangeAtOffset(i);
				if (range == null)
					style = new StyleRange(i, 1, null, null, SWT.NORMAL);
				else {
					style = (StyleRange) range.clone();
					style.start = i;
					style.length = 1;
				}
				style.strikeout = !style.strikeout;
				infoVaultInformation.getInformationEditor(current).setStyleRange(style);
			}
			infoVaultUI.markInformationAsModified(current);
		}
	};

	static final SelectionAdapter listenerColorForeGround = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			ColorDialog cd = new ColorDialog(infoVaultUI.shell);
			cd.setText(infoVault.sTitle);
			cd.setRGB(new RGB(255, 255, 255));
			RGB newColor = cd.open();
			if (newColor == null) return;
			int current = infoVaultUI.getCurrentPShelf();
			setColor(newColor);
			infoVaultUI.markInformationAsModified(current);
		}
	};

	static final SelectionAdapter listenerColorBackGround = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			ColorDialog cd = new ColorDialog(infoVaultUI.shell);
			cd.setText(infoVault.sTitle);
			cd.setRGB(new RGB(255, 255, 255));
			RGB newColor = cd.open();
			if (newColor == null) return;
			int current = infoVaultUI.getCurrentPShelf();
			infoVaultInformation.getInformationEditor(current).setBackground(new Color(Display.getCurrent(), newColor));
			infoVaultUI.markInformationAsModified(current);
		}
	};

	static final SelectionAdapter listenerImage = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			FileDialog dlg = new FileDialog(infoVaultUI.shell);
			dlg.setText(infoVault.sTitle);
			dlg.setFilterExtensions(new String[] { "*.jpg;*.gif;*.png" });
			dlg.setFilterNames(new String[] { "Image file (jpg, gif, png)" });
			dlg.setFilterPath(System.getProperty("user.home"));
			String filename = dlg.open();
			if (filename == null) return;
			try {
				Image image = infoVaultCacheImages.get(filename);
				int i = filename.lastIndexOf('.');
				String extension = "";
				if (i > 0) extension = filename.substring(i+1);
				int type;
				switch (extension) {
					case "jpg": type = SWT.IMAGE_JPEG; break;
					case "gif": type = SWT.IMAGE_GIF; break;
					case "png": type = SWT.IMAGE_PNG; break;
					//case "ico": type = SWT.IMAGE_ICO; break;
					default: type = SWT.IMAGE_PNG; break;
				}
				int current = infoVaultUI.getCurrentPShelf();
				int offset = infoVaultInformation.getInformationEditor(current).getCaretOffset();
				infoVaultInformation.getInformationEditor(current).replaceTextRange(offset, 0, MARKER);
				infoVaultInformation.addImage(current, offset, type, image);
				infoVaultUI.insertImage(infoVaultInformation.getInformationEditor(current), offset, image);
				infoVaultUI.markInformationAsModified(current);
			} catch (Exception ex) {
				infoVaultUI.displayError(ex.getMessage(), 0);
			}
		}
	};

	static final SelectionAdapter listenerUrl = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			int current = infoVaultUI.getCurrentPShelf();
			Point sel = infoVaultInformation.getInformationEditor(current).getSelectionRange();
			if (sel == null || sel.y == 0) return;
			StyleRange style;
			for (int i = sel.x; i < sel.x + sel.y; ++i) {
				StyleRange range = infoVaultInformation.getInformationEditor(current).getStyleRangeAtOffset(i);
				if (range == null)
					style = new StyleRange(i, 1, null, null, SWT.NORMAL);
				else {
					style = (StyleRange) range.clone();
					style.start = i;
					style.length = 1;
				}
				style.underline = true;
				style.underlineStyle = SWT.UNDERLINE_LINK;
				style.foreground = infoVaultLogOn.URLCOLOR;
				style.data = infoVaultInformation.getInformationEditor(current).getSelectionText();
				infoVaultInformation.getInformationEditor(current).setStyleRange(style);
			}
			infoVaultUI.markInformationAsModified(current);
		}
	};
	//--------------------------------------------------------------------------
	private static void setColor(RGB newColor) {
		int current = infoVaultUI.getCurrentPShelf();
		Point sel = infoVaultInformation.getInformationEditor(current).getSelectionRange();
		if ((sel == null) || (sel.y == 0)) return;
		StyleRange style;
		for (int i = sel.x; i < sel.x + sel.y; ++i) {
			StyleRange range = infoVaultInformation.getInformationEditor(current).getStyleRangeAtOffset(i);
			if (range == null)
				style = new StyleRange(i, 1, null, null, SWT.NORMAL);
			else {
				style = (StyleRange) range.clone();
				style.start = i;
				style.length = 1;
			}
			style.foreground = infoVaultCacheColors.get(newColor);
			infoVaultInformation.getInformationEditor(current).setStyleRange(style);
		}
	}
	//--------------------------------------------------------------------------
	private static void setFont(FontData fd) {
		int current = infoVaultUI.getCurrentPShelf();
		if (0 == infoVaultInformation.getInformationEditor(current).getSelectionCount()) {
			infoVaultInformation.getInformationEditor(current).setFont(infoVaultCacheFonts.add(fd));
			return;
		}
		Point sel = infoVaultInformation.getInformationEditor(current).getSelectionRange();
		if ((sel == null) || (sel.y == 0)) return;
		StyleRange style;
		for (int i = sel.x; i < sel.x + sel.y; ++i) {
			StyleRange range = infoVaultInformation.getInformationEditor(current).getStyleRangeAtOffset(i);
			if (range == null)
				style = new StyleRange(i, 1, null, null, SWT.NORMAL);
			else {
				style = (StyleRange) range.clone();
				style.start = i;
				style.length = 1;
			}
			style.font = infoVaultCacheFonts.add(fd);
			infoVaultInformation.getInformationEditor(current).setStyleRange(style);
		}
		infoVaultInformation.getInformationEditor(current).setSelectionRange(sel.x + sel.y, 0);
	}
}
//==============================================================================
