//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

import org.eclipse.nebula.widgets.pshelf.PShelf;
import org.eclipse.nebula.widgets.pshelf.PShelfItem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.GlyphMetrics;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.*;

import java.util.Arrays;
//------------------------------------------------------------------------------
class infoVaultUI {
	//--------------------------------------------------------------------------
	static class anInformationHeader implements Comparable<Object> {
		private String header;
		private int type;

		String getHeader() { return header; }
		void setHeader(String header) { this.header = header; }
		int getType() { return type; }
		void setType(int type) { this.type = type; }
		public int compareTo(Object o) {
			return o == null ? -1 : header.compareTo(((anInformationHeader) o).header);
		}
	}
	//--------------------------------------------------------------------------
	private static Button saveInformationButton, removeInformationButton;
	private static int newInformationEditorCounter;
	private static int newInformationGridCounter;
	private static PShelf shelf;

	private static final String NEWINFO_EDITOR = "-- New Editor";
	private static final String NEWINFO_TABLE = "-- New Grid";
	//private static final String GALLERY = "IMAGE GALLERY";
	private static final String BUTTON_NEW_TEXT = "New Tab / Text";
	private static final String BUTTON_NEW_TABLE = "New Tab / Grid";
	private static final String BUTTON_REMOVE = "Remove Tab";
	private static final String BUTTON_SAVE = "Save Tab";

	private static final String ITEM_BOLD = "Bold";
	private static final String ITEM_ITALIC = "Italic";
	private static final String ITEM_UNDERLINE = "Underline";
	private static final String ITEM_STRIKEOUT = "Strikeout";
	private static final String ITEM_FOREGROUND = "Font Color";
	private static final String ITEM_BACKGROUND = "Background Color";
	private static final String ITEM_IMAGE = "Image";
	private static final String ITEM_URL = "Url";
	private static final String ITEM_EXPORT = "Export Data";
	private static final String ITEM_IMPORT = "Import Data";
	private static final String ITEM_SORT = "Alphabetical Sorting according to the 1st column";
	//--------------------------------------------------------------------------
	static Shell shell;

	static final String ASTERISK = "*";
	static final boolean MODIFIED = true;
	static final boolean EXISTING = true;
	static final int TYPE_INFORMATION_EDITOR = 0;
	//static final int TYPE_INFORMATION_TABLE = 1;
	//private static final int TYPE_INFORMATION_GALLERY = 2;
	static final int TYPE_INFORMATION_GRID = 3;
	//--------------------------------------------------------------------------
	static void vaultShell(Display d) {
		shell = new Shell(d, SWT.MAX | SWT.MIN | SWT.CLOSE | SWT.TITLE | SWT.RESIZE);
		shell.setText(infoVault.sTitle);
		shell.setLayout(new GridLayout());
		shell.setCursor(d.getSystemCursor(SWT.CURSOR_ARROW));
		shell.setBackgroundMode(SWT.INHERIT_DEFAULT);
		shell.addListener(SWT.Close, (Event e) -> {
			for (int i = 0; i < infoVaultInformation.size(); ++i)
				e.doit = shouldWeSaveIt(infoVaultInformation.getInformationNameUpdated(i), i);
		});

		pshelvesArea(shell);
		buttonsArea(shell);

		shell.pack();
		shell.setSize(800, 800);
		Rectangle shellRect = shell.getBounds();
		Rectangle displayRect = d.getBounds();
		shell.setLocation((displayRect.width - shellRect.width) / 2, (displayRect.height - shellRect.height) / 2);
		shell.open();
		while (!shell.isDisposed())
			if (!d.readAndDispatch())
				d.sleep();
	}
	//--------------------------------------------------------------------------
	private static void pshelvesArea(Shell parent) {
		final GridData gd = new GridData();
		gd.grabExcessVerticalSpace = gd.grabExcessHorizontalSpace = true;
		gd.horizontalAlignment = GridData.FILL;
		gd.horizontalIndent = 0;
		gd.verticalAlignment = GridData.FILL;
		gd.verticalIndent = 0;

		shelf = new PShelf(parent, SWT.SIMPLE);
		// Optionally, change the renderer
		//shelf.setRenderer(new RedmondShelfRenderer());
		shelf.setLayoutData(gd);

		anInformationHeader[] listInformation = listInformationSort(infoVaultDB.getHeaders());

		//**** REMOVE AUTOMATIC ADDING OF A GALLERY LISTING ALL IMAGES USED IN THE VAULT.
		//PShelfItem galleryItem = createPShelf(shelf, GALLERY, TYPE_INFORMATION_GALLERY);
		//infoVaultGallery.displayGallery(galleryItem.getBody(), listInformation);
		//infoVaultInformation.add(GALLERY, galleryItem, TYPE_INFORMATION_GALLERY, EXISTING);
		//****

		for (anInformationHeader info : listInformation) {
			PShelfItem item = createPShelf(shelf, info.getHeader(), info.getType());
			displayInformation(item, infoVaultInformation.add(info.getHeader(), item, info.getType(), EXISTING));
		}

		shelf.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				String currentItem = ((PShelfItem)event.item).getText();
				if (currentItem.length() == 0) return;
				int index = infoVaultInformation.indexOf(currentItem);
				if (index == -1) {
					displayError("Abnormal situation: Non existent PShelf item!!!", 2);
					infoVault.Exit(true, -1);
				}
				//**** REMOVE AUTOMATIC ADDING OF A GALLERY LISTING ALL IMAGES USED IN THE VAULT.
				//if (currentItem.equals(GALLERY)) {
				//	infoVaultGallery.refreshGallery(listInformationSort(infoVaultDB.getHeaders()));
				//	removeInformationButton.setEnabled(false);
				//	saveInformationButton.setEnabled(false);
				//} else {
					if (infoVaultInformation.getInformationType(index) == TYPE_INFORMATION_EDITOR) {
						Control[] ctrl = ((PShelfItem)event.item).getBody().getChildren();
						for (Control aCtrl : ctrl) {
							Control[] ctrl2 = ((Composite) aCtrl).getChildren();
							for (Control aCtrl2 : ctrl2) aCtrl2.redraw();
						}
					}
					removeInformationButton.setEnabled(infoVaultInformation.doesInformationExist(index));
					saveInformationButton.setEnabled(infoVaultInformation.hasInformationBeenChanged(index));
				//}
			}
		});
	}

	private static PShelfItem createPShelf(PShelf parent, String name, int type) {
		PShelfItem item = new PShelfItem(parent, SWT.NONE);
		item.setText(name);
		switch (type) {
			case TYPE_INFORMATION_EDITOR:
				item.setImage(infoVaultLogOn.textImage);
				item.getBody().setLayout(new GridLayout());
				break;
			case TYPE_INFORMATION_GRID:
				item.setImage(infoVaultLogOn.tableImage);
				item.getBody().setLayout(new GridLayout());
				break;
			//**** REMOVE AUTOMATIC ADDING OF A GALLERY LISTING ALL IMAGES USED IN THE VAULT.
			//case TYPE_INFORMATION_GALLERY:
			//	item.setImage(infoVaultLogOn.LogoImageSmall);
			//	item.getBody().setLayout(new FillLayout());
			//	break;
			//****
			default:
				break;
		}
		return item;
	}
	//--------------------------------------------------------------------------
	private static void buttonsArea(Shell parent) {
		final GridLayout layout = new GridLayout();
		layout.numColumns = 5;
		layout.makeColumnsEqualWidth = true;

		final GridData data = new GridData(SWT.FILL, SWT.BOTTOM, true, false);
		data.grabExcessHorizontalSpace = true;
		data.grabExcessVerticalSpace = false;

		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(layout);
		container.setLayoutData(data);

		Button editorInformationButton = createButton(container, BUTTON_NEW_TEXT, true);
		editorInformationButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				++newInformationEditorCounter;
				String name = NEWINFO_EDITOR + " " +  + newInformationEditorCounter;
				PShelfItem item = createPShelf(shelf, name, TYPE_INFORMATION_EDITOR);
				int index = infoVaultInformation.add(name, item, TYPE_INFORMATION_EDITOR, ! EXISTING);
				displayInformation(item, index);
				shelf.setSelection(item);
			}
		});

		Button tableInformationButton = createButton(container, BUTTON_NEW_TABLE, true);
		tableInformationButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				++newInformationGridCounter;
				String name = NEWINFO_TABLE + " " +  +newInformationGridCounter;
				PShelfItem item = createPShelf(shelf, name, TYPE_INFORMATION_GRID);
				int index = infoVaultInformation.add(name, item, TYPE_INFORMATION_GRID, ! EXISTING);
				displayInformation(item, index);
				shelf.setSelection(item);
			}
		});

		Label invisibleLabel = new Label(container, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		invisibleLabel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));

		removeInformationButton = createButton(container, BUTTON_REMOVE, false);
		removeInformationButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if ((e.doit = removingNotConfirmed())) return;
				int index = getCurrentPShelf();
				infoVaultDB.deleteInformation(infoVaultInformation.getInformationName(index));
				infoVaultInformation.remove(index);
			}
		});

		saveInformationButton = createButton(container, BUTTON_SAVE, false);
		saveInformationButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				int index = getCurrentPShelf();
				if (infoVaultInformation.doesInformationExist(index)) {
					saveInformationButton.setEnabled(false);
					infoVaultDB.saveInformation(infoVaultLogOn.encryptionKey, index);
					infoVaultInformation.set(index,
							infoVaultInformation.getInformationNameUpdated(index),
							infoVaultInformation.getInformationNameUpdated(index),
							infoVaultInformation.getInformationType(index),
							!MODIFIED, EXISTING);
					if (!infoVaultInformation.getInformationName(index).equals(infoVaultInformation.getInformationNameUpdated(index)))
						infoVaultInformation.getInformationPShelfItem(index).setText(infoVaultInformation.getInformationNameUpdated(index));
				} else {
					int k1 = Math.min(NEWINFO_EDITOR.length(), infoVaultInformation.getInformationNameUpdated(index).length());
					int k2 = Math.min(NEWINFO_TABLE.length(), infoVaultInformation.getInformationNameUpdated(index).length());
					if (infoVaultInformation.getInformationName(index).length() == 0
							|| infoVaultInformation.getInformationName(index).substring(0, k1).equals(NEWINFO_EDITOR)
							|| infoVaultInformation.getInformationName(index).substring(0, k2).equals(NEWINFO_TABLE)) {
						displayWarning("Please, specify a name for this valuable information!");
						return;
					}
					infoVaultInformation.set(index,
							infoVaultInformation.getInformationNameUpdated(index),
							infoVaultInformation.getInformationNameUpdated(index),
							infoVaultInformation.getInformationType(index),
							!MODIFIED, EXISTING);
					saveInformationButton.setEnabled(false);
					infoVaultDB.insertInformation(index);
				}
				infoVaultInformation.getInformationPShelfItem(index).setText(infoVaultInformation.getInformationName(index));
				removeInformationButton.setEnabled(true);
			}
		});
	}
	private static Button createButton(Composite parent, String name, boolean enabled) {
		Button button = new Button(parent, SWT.PUSH);
		button.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, true, 1, 1));
		button.setText(name);
		button.setFont(infoVaultLogOn.fontButton);
		button.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		button.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		button.setEnabled(enabled);
		return button;
	}
	//==========================================================================
	private static void displayInformation(PShelfItem item, int index) {
		Composite container = new Composite(item.getBody(), SWT.NONE);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		container.setLayout(new GridLayout(1, false));

		Composite contInfoName = new Composite(container, SWT.FILL);
		contInfoName.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 0, 0));
		contInfoName.setLayout(new GridLayout(3, false));
		titleArea(contInfoName, infoVaultInformation.getInformationName(index), item);
		//----------------------------------------------------------------------
		switch (infoVaultInformation.getInformationType(index)) {
			case TYPE_INFORMATION_EDITOR:
				showEditorCapabilities(container);
				infoVaultInformation.setInformationEditor(index, infoVaultEditor.editorArea(container, index));
				infoVaultInformation.getInformationEditor(index).addListener(SWT.MouseDown, event -> {
					// Follow url on CTRL / Mouse Click
					if ((event.stateMask & SWT.MOD1) != 0) {
						try {
							int current = infoVaultUI.getCurrentPShelf();
							int offset = infoVaultInformation.getInformationEditor(current).getOffsetAtPoint(new Point(event.x, event.y));
							StyleRange style1 = infoVaultInformation.getInformationEditor(current).getStyleRangeAtOffset(offset);
							if (style1 != null && style1.underline && style1.underlineStyle == SWT.UNDERLINE_LINK) {
								StyleRange[] style2 = infoVaultInformation.getInformationEditor(current).getStyleRanges();
								for (StyleRange st : style2) {
									if ((style1.start < st.start) || style1.start >= st.start + st.length) continue;
									Program.launch(infoVaultInformation.getInformationEditor(current).getText(st.start, st.start + st.length - 1));
								}
							}
						} catch (IllegalArgumentException e) {
							// no character under event.x, event.y
						}
					}
				});
				break;
			case TYPE_INFORMATION_GRID:
				showGridCapabilities(container);
				infoVaultInformation.setInformationGrid(index, infoVaultGrid.gridArea(container, index));
				break;
			default:
				displayError("Abnormal situation: Incorrect information type", 2);
				infoVault.Exit(true, -1);
				break;
		}
	}
	//--------------------------------------------------------------------------
	private static void showEditorCapabilities(Composite parent) {
		CoolBar toolBar = new CoolBar(parent, SWT.NONE);
		createToolItem(toolBar, infoVaultLogOn.boldImage, ITEM_BOLD, infoVaultEditor.listenerStyleBold);
		createToolItem(toolBar, infoVaultLogOn.italicImage, ITEM_ITALIC, infoVaultEditor.listenerStyleItalic);
		createToolItem(toolBar, infoVaultLogOn.underlineImage, ITEM_UNDERLINE, infoVaultEditor.listenerStyleUnderline);
		createToolItem(toolBar, infoVaultLogOn.strikeoutImage, ITEM_STRIKEOUT, infoVaultEditor.listenerStyleStrikeOut);
		createToolItem(toolBar, infoVaultLogOn.foregroundImage, ITEM_FOREGROUND, infoVaultEditor.listenerColorForeGround);
		createToolItem(toolBar, infoVaultLogOn.backgroundImage, ITEM_BACKGROUND, infoVaultEditor.listenerColorBackGround);
		createToolItem(toolBar, infoVaultLogOn.imagesImage, ITEM_IMAGE, infoVaultEditor.listenerImage);
		createToolItem(toolBar, infoVaultLogOn.urlsImage, ITEM_URL, infoVaultEditor.listenerUrl);
		toolBar.pack();
	}

	private static void showGridCapabilities(Composite parent) {
		CoolBar toolBar = new CoolBar(parent, SWT.NONE);
		createToolItem(toolBar, infoVaultLogOn.boldImage, ITEM_BOLD, infoVaultGrid.listenerStyleBold);
		createToolItem(toolBar, infoVaultLogOn.italicImage, ITEM_ITALIC, infoVaultGrid.listenerStyleItalic);
		createToolItem(toolBar, infoVaultLogOn.foregroundImage, ITEM_FOREGROUND, infoVaultGrid.listenerColorForeGround);
		createToolItem(toolBar, infoVaultLogOn.backgroundImage, ITEM_BACKGROUND, infoVaultGrid.listenerColorBackGround);
		createToolItem(toolBar, infoVaultLogOn.urlsImage, ITEM_URL, infoVaultGrid.listenerUrl);
		createToolItem(toolBar, infoVaultLogOn.exportImage, ITEM_EXPORT, infoVaultGrid.listenerExport);
		createToolItem(toolBar, infoVaultLogOn.importImage, ITEM_IMPORT, infoVaultGrid.listenerImport);
		createToolItem(toolBar, infoVaultLogOn.sortImage, ITEM_SORT, infoVaultGrid.listenerSort);
		toolBar.pack();
	}

	private static void createToolItem(CoolBar tb, Image image, String text, SelectionAdapter lstner) {
		CoolItem item = new CoolItem(tb, SWT.NONE);
		Button button = new Button (tb, SWT.PUSH);
		button.setImage(image);
		button.setToolTipText(text);
		button.setEnabled(true);
		button.setBackground(tb.getBackground());
		button.addSelectionListener(lstner);

		Point size = button.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		item.setPreferredSize (item.computeSize(size.x, size.y));
		item.setControl(button);
	}

	//--------------------------------------------------------------------------
	private static void titleArea(Composite parent, String name, PShelfItem i) {
		Label lab = new Label(parent, SWT.NONE);
		lab.setText("Name: ");
		lab.setFont(infoVaultLogOn.fontLabel);
		lab.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));

		final GridData gd1 = new GridData();
		gd1.grabExcessHorizontalSpace = true;
		gd1.horizontalAlignment = GridData.FILL;
		gd1.grabExcessVerticalSpace = false;
		gd1.verticalAlignment = GridData.FILL;

		Text $ = new Text(parent, SWT.BORDER);
		$.setLayoutData(gd1);
		$.setFont(infoVaultLogOn.fontText);
		$.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		$.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		$.forceFocus();
		$.setText(name);
		$.addTraverseListener(e -> {
			if (e.detail == SWT.TRAVERSE_RETURN) {
				e.doit = false;
				e.detail = SWT.TRAVERSE_NONE;
			}
		});

		Button ok = new Button(parent, SWT.PUSH);
		ok.setText("OK");
		ok.setFont(infoVaultLogOn.fontButton);
		ok.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		ok.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		ok.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 0, 0));
		ok.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				int current = getCurrentPShelf();
				String name = $.getText();
				if (infoVaultInformation.indexOf(name) != -1) {
					displayError("This name already exist. Try another one...", 0);
					$.setText(infoVaultInformation.getInformationPShelfItem(current).getText());
				} else {
					i.setText(ASTERISK + name);
					infoVaultInformation.set(current, infoVaultInformation.getInformationName(current), name,
							infoVaultInformation.getInformationType(current),
							MODIFIED, infoVaultInformation.doesInformationExist(current));
					saveInformationButton.setEnabled(true);
				}
			}
		});
	}
	//--------------------------------------------------------------------------
	static void markInformationAsModified(int itab) {
		(infoVaultInformation.getInformationPShelfItem(itab)).setText(ASTERISK + infoVaultInformation.getInformationNameUpdated(itab));
		infoVaultInformation.set(getCurrentPShelf(), infoVaultInformation.getInformationNameUpdated(itab),
				infoVaultInformation.getInformationNameUpdated(itab),
				infoVaultInformation.getInformationType(itab),
				MODIFIED, infoVaultInformation.doesInformationExist(itab));
		saveInformationButton.setEnabled(true);
	}
	//--------------------------------------------------------------------------
	static int getCurrentPShelf() {
		return infoVaultInformation.indexOf(shelf.getSelection().getText());
	}
	//==========================================================================
	static void insertImage(StyledText editor, int offset, Image img) {
		StyleRange style = new StyleRange();
		style.start = offset;
		style.length = 1;
		style.data = img;
		Rectangle rect = img.getBounds();
		style.metrics = new GlyphMetrics(rect.height, 0, rect.width);
		editor.setStyleRange(style);
	}
	//==========================================================================
	static void displayError(String error, int additionalInformation) {
		MessageBox messageBox = new MessageBox(shell, SWT.OK | SWT.ICON_ERROR);
		switch (additionalInformation) {
			case 1:
				error += "\n\nThe session will be aborted.\n"
						+ "Suggestion: Report the problem by describing the use case as accurately as possible.";
				break;
			case 2:
				error += "\n\nSuggestion: Report the problem by describing the use case as accurately as possible.";
				break;
			default:
				break;
		}
		messageBox.setMessage(error);
		messageBox.open();
	}
	//--------------------------------------------------------------------------
	static void displayWarning(String warning) {
		MessageBox messageBox = new MessageBox(shell, SWT.OK | SWT.ICON_WARNING);
		messageBox.setMessage(warning);
		messageBox.open();
	}
	//--------------------------------------------------------------------------
	private static int displayQuestion(String question) {
		MessageBox messageBox = new MessageBox(shell, SWT.NO | SWT.YES | SWT.ICON_WARNING);
		messageBox.setText("Warning");
		messageBox.setMessage(question);
		return messageBox.open();
	}
	//--------------------------------------------------------------------------
	private static boolean shouldWeSaveIt(String name, int idx) {
		if (!infoVaultInformation.hasInformationBeenChanged(idx))
			return true;
		int j = displayQuestion(name + ": Would you like to save yours changes before closing?");
		if (j == SWT.CANCEL)
			return false;
		if (j == SWT.YES) {
			if (infoVaultInformation.doesInformationExist(idx)) {
				infoVaultInformation.set(idx, name, name,
						infoVaultInformation.getInformationType(idx),
						!MODIFIED, EXISTING);
				saveInformationButton.setEnabled(false);
				infoVaultInformation.getInformationPShelfItem(idx).setText(name);
				infoVaultDB.saveInformation(infoVaultLogOn.encryptionKey, idx);
			} else {
				int k1 = Math.min(NEWINFO_EDITOR.length(), infoVaultInformation.getInformationNameUpdated(idx).length());
				int k2 = Math.min(NEWINFO_TABLE.length(), infoVaultInformation.getInformationNameUpdated(idx).length());
				if (infoVaultInformation.getInformationName(idx).length() == 0
						|| infoVaultInformation.getInformationName(idx).substring(0, k1).equals(NEWINFO_EDITOR)
						|| infoVaultInformation.getInformationName(idx).substring(0, k2).equals(NEWINFO_TABLE)) {
					displayWarning("Please, specify a name for this valuable information!");
					return false;
				}
				infoVaultInformation.set(idx, name, name,
						infoVaultInformation.getInformationType(idx),
						!MODIFIED, EXISTING);
				saveInformationButton.setEnabled(false);
				infoVaultInformation.getInformationPShelfItem(idx).setText(name);
				infoVaultDB.insertInformation(idx);
			}
		}
		return true;
	}
	//--------------------------------------------------------------------------
	private static boolean removingNotConfirmed() {
		int j = displayQuestion("Please confirm the deletion of this information.");
		return (j == SWT.CANCEL || j == SWT.NO);
	}
	//--------------------------------------------------------------------------
	private static anInformationHeader[] listInformationSort(anInformationHeader[] ih) {
		Arrays.sort(ih);
		return ih;
	}
	//==========================================================================
	static void Exit() { }
}
//==============================================================================
