# RELEASE NOTES: *infoVault*, Management of sensitive information at home

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 4.10.11**:
  - Updated versions of third party jar files.

- **Version 4.10.10**:
  - Updated build system components.

- **Version 4.10.9**:
  - Updated build system.

- **Version 4.10.8**:
  - Removed unused files.

- **Version 4.10.7**:
  - Updated build system component(s)

- **Version 4.10.6**:
  - Reworked build system to ease global and inter-project updated.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 4.10.5**:
  - Abandonned Windows support since version 4.10.3

- **Version 4.10.4**:
  - Updated SWT library from 4.12 to 4.15.

- **Version 4.10.3**:
  - Some minor changes in project structure and build.

- **Version 4.10.2 - Build 108**:
  - Added Application version at java level.

- **Version 4.10.1 - Build 107**:
  - Removed *Fonts* button. We are only using Arial.
  - Fixed *Bold* and *Italic* in Grid panel: they now act as a flipflop.

- **Version 4.10.0 - Build 106**:
  - Updated versions of third party jar files.
  - Removed management of first blocked column used for line numbering.
  - Removed management of first blocked line simulating column header.
  - Added features in Grid panel: column header editor for naming column.
  - Added features in Grid panel: Export ALL data in a csv file.
  - Added features in Grid panel: Import data from a csv file.
  - Added features in Grid panel: Deletion of several lines.
  - Added features in Grid panel: Apply bold, italic, foreground, background and links on all selected lines in the selected column.
  - Changed icons (and increased their size: 16x16 -> 32x32).

- **Version 4.9.1 - Build 105**:
  - Improved /infoVault_bin/Makefile and ./infoVault_c/Makefile.

- **Version 4.9.0 - Build 105**:
  - Improved ./Makefile, ./infoVaul_bin/Makefile, ./infoVault_c/Makefile, ./infoVault_java/Makefile.
  - Removed C compilation warnings.
  - Moved to SWT 4.12. Initially was 4.9.
  - Moved to SQLIT JDBC 3.27.2.1. Initially was 3.23.1.
  - Moved to Eclise Nebula 2.2.0 2019. Initially was 2.1.0 2018.
  - Moved to JFACE jface_3.17.0.v20190820-1444 (Eclipse 019.09).
  - Updated README file.

- **Version 4.8.3 - Build 104**:
  - Move to SWT 4.9.
  - Move to JFACE 3.14.100.v20180828-0836.

- **Version 4.8.2 - Build 103**:
  - Some omissions...

- **Version 4.8.1 - Build 103**:
  - Removed generated jar files on *clean*.
  - Automated, as much as possible, the management of java libraries to facilitate their update.
  - Added version identification text file.

- **Version 4.8.0 - Build 102**:
  - Updated libraries to their most recent versions: Eclipse Photon and Nebula 2.0.1
  - Moved from GPL v2 to GPL v3.
  - Replaced license files (COPYNG and LICENSE) by markdown version.
  - Replaced Release Nores file (this file) by markdown version.
  - Removed *index.html* file (was used for *GitHub*).
  - Replaced use of deprecated API org.eclipse.swt.custom.StyledText.getOffsetAtLocation(Point) by StyledText.getOffsetAtPoint(Point)
  - Fixed issue with background color in Tool bar (edit o grid capabilities): replace ToolBar by CoolBar.*
  - Updated README.md file.

- **Version 4.7.2 - Build 101**:
  - Updated "README.md" file about the copyright of the image used as wallpaper.

- **Version 4.7.1 - Build 101**:
  - Change the Pshell renderer.

- **Version 4.7.0 - Build 100**:
  - Removed gallery display (corresponding source code commented).
  - Redesign the splashscreen.

- **Version 4.6.0 - Build 96**:
  - Move to Eclipse Jface_3.13.0.20170503-1507
  - Move to Nebule / Gallery 1.0.0.201706221838
  - Move to Nebule / Grid 1.0.0.201706221838
  - Move to Nebule / Pshelf 1.1.0.201706221838
  - Move to sqlite-jdbc 3.19.3
  - Move to SWT 4.7

- **Version 4.5.0 - Build 95**:
  - Added ".comment" file in each directory for 'yaTree' utility.

- **Version 4.4.2 - Build 95**:
  - Fixed typos and omission in README file.

- **Version 4.4.1 - Build 95**:
	Slightly improved C sources and Makefiles.

- **Version 4.4.0 - Build 95**:
  - Driver is now a C executable. Was a script (bash / cmd) file.
  - Introduced cross generation for Windows (make wall|wclean|winstall|wcleaninstall).

- **Version 4.3.2 - Build 94**:
  - Fix Markdown syntax in README file (headers)

- **Version 4.3.1 - Build 94**:
  - Specified foreground and background colors after migration from Ubuntu/Mate
	  to XUbuntu/Xfce4 (slightly differents with both 2 window managers).

- **Version 4.3.1 - Build 93**:
  - Slightly modified pshelf appearance.

- **Version 4.3.1 - Build 92**:
  - Move to SWT 4.6.3. Initially was 4.6.2.
  - Move to Commons CLI 1.4 Initially was 1.3.1.

- **Version 4.3.0 - Build 92**:
  - Moved to Nebula 1.2.0. Initially it was 1.1.0.
  - Added "Bold" and "Italic" capabilities for Grid cell (either one or the other).
  - Fix: Issue with values containing "," character in Grid.

- **Version 4.2.0 - Build 90**:
  - Replaced table information by grid information.

```
	*** WARNING *** This new version is incompatible with the previous one. A new 
	                database will be created. Backup your information using the previous
					version to re-inject it into the new database.
```

- **Version 4.1.0 - Build 86**:
  - Fix: font and foregroung color for column header not saved.

- **Version 4.1.0 - Build 85**:
  - Tentative to add image management in table information. Aborted...

- **Version 4.1.0 - Build 84**:
  - Added URL management in table information.

- **Version 4.1.0 - Build 83**:
  - Added URL management in text information.

- **Version 4.1.0 - Build 82**:
  - Improved edition capabilities in table.

*- *Version 4.1.0 - Build 81**:
  - Added background & foreground colors and font management for table cells.

- **Version 4.0.0 - Build 80**:
  - Replaced tab folder by "PShelf" from SWT/Nebula project.

- **Version 3.1.1 - Build 76**:
  - Minor fixes and enhancements.

- **Version 3.1.0 - Build 75**:
  - Move to latest version of library 'sqlite-jdbc' v3.16.1 (was 3.8.11.2).

- **Version 3.0.0 - Build 75**:
  - Added information tab based on multi-column table templates.
  - Removed clipborad buttons. CTRL-A, CTRL-C and CTRL-V work fine.
  - Associated edition capabilities buttons to information tab based on editor
  - Added a "Image Gallery" as first tab, systematically displayed and grouping,
	  in thumbnail form, all the images inserted in information. By right-clicking
	  on a thumbnail, the corresponding image can be exported as PNG file.

```
	*** WARNING *** This new version is incompatible with the previous one. A new 
	                database will be created. Backup your information using the previous
					version to re-inject it into the new database.
```

- **Version 2.3.0 - Build 60**:
  - Re-looked the graphical interface. Documentation updated.
  - Added icon on main panel.
  - Renamed "icons" directory as "images".
  - Encryption key: 'return' is now equivalent to "OK".

- **Version 2.2.0 - Build 54**:
  - Information list is displayed sorted alphabetically.
  - Implemented rename of an information.

- **Version 2.2.0 - Build 52**:
  - Added a confirmation request panel before removing an information.
   - Fixed availability of the "*Remove Information*" button.

- **Version 2.2.0 - Build 50**:
  - The change of character font now applies to the selected text or to the entire text if there is no selection
  - Move to SWT (Standard Widget Toolkit) 4.6.2 (previous: 4.6.1).
  - Rename "Clean Clipboard" to "Clear Clipboard".

- **Version 2.1.0 - Build 47**:
  - Fixed wrong contents initoalization for clipboard

- **Version 2.1.0 - Build 46**:
  - Ooops... Set clipboard buttons to "visible".

- **Version 2.1.0 - Build 45**:
  - Added "Clipboard" buttons.

- **Version 2.0.4 - Build 42**:
  - Minor fixes in Makefile, README, ....

- **Version 2.0.3 - Build 42**:
  - Added basic Windows installation script.

- **Version 2.0.2 - Build 42**:
  - Fixed the bug reported in the note of the "build 41".

- **Version 2.0.1 - Build 41**:
  - Found a problem (on Windows) in reading information with a complex mixture of styles. Implemented a quick fix to avoid crash. Definitive correction to come.

- **Version 2.0.0 - Build 40**:
  - Added basic edition capabilities: bold, italic, underline, strike out.
  - Added color capabilities: foregroung, background.
  - Added image capabilities: PNG, JPEG, GIF files insertion.
  - Added keybindings support:  select all (ctrl A). Ctrl C, ctrl V, ctrl X are natively supported.
  - Moved from ECLIPSE Neon.1 Release (4.6.1)  to IntelliJ IDEA 2016.3.2.

- **Version 1.1.0 - Build 25**:
  - Refactored of Java source code following SPARTAN's suggestion, making it shorter, more efficient and more readable.
  - Added 'Change Encryption Key' feature.

- **Version 1.0.0 - Build 20**:
  - First release.
